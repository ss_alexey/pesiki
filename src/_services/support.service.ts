import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../environments/environment';
import {
  GetTicketOptions,
  SupportTickets,
  SupportMessage,
  SuppportTicketInfo,
  SupportAttachment
} from '../models';
import { Observable } from 'rxjs';
import { Store, Section } from '../app/store';

const baseUrl = environment.baseUrl;

@Injectable({
  providedIn: 'root'
})
export class SupportService {
  constructor(private http: HttpClient, private store: Store) {}

  getCategories() {
    return this.http.get(`${baseUrl}/support/getCategories`);
  }

  getTickets(params: GetTicketOptions = {}): Observable<SupportTickets> {
    return this.http.post<SupportTickets>(`${baseUrl}/support/getTickets/`, {
      ...params,
      email: this.userEmail
    });
  }

  getTicketById(id: number): Observable<SuppportTicketInfo> {
    return this.http.get<SuppportTicketInfo>(
      `${baseUrl}/support/getTicketById/${id}`
    );
  }

  createTicket(ticket) {
    console.log({
      ...ticket,
      email: this.userEmail
    });
    return this.http.post(`${baseUrl}/support/createTicket`, {
      ...ticket,
      email: this.userEmail
    });
  }

  sendMessage(message): Observable<SupportMessage> {
    return this.http.post<any>(`${baseUrl}/support/sendMessage/`, message);
  }

  getTopicks(categoryId) {
    return this.http.get(
      `${baseUrl}/support/getTopicsByCategory/${categoryId}/`
    );
  }

  getAllTopics() {
    return this.http.get(`${baseUrl}/support/getAllTopics`);
  }

  getProblems(topicId) {
    return this.http.get(`${baseUrl}/support/getProblemsByTopic/${topicId}`);
  }

  uploadAttachment(
    fileId: number,
    file: { base64: string }
  ): Observable<SupportAttachment> {
    return this.http.put<SupportAttachment>(
      `${baseUrl}/support/uploadAttachment/${fileId}`,
      file
    );
  }

  private get userEmail() {
    return this.store.value[Section.user]['email'];
  }
}
