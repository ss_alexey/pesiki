import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../environments/environment';
import { Observable } from 'rxjs';
// import { Store, Section } from '@app/store';
import { Store, Section } from '../app/store';
import { first } from 'rxjs/operators';

export interface TwoFactorAction {
  name: string;
  status: number;
  options: TwoFactorActionOption[];
}

interface TwoFactorActionOption {
  key: string;
}

export interface TwoFactorMethods {
  type: string;
  status: number;
}
export interface TwoFactorSettings {
  id?: number;
  secret: string;
  type: string;
  action: string;
  state: number;
  options: TwoFactorSettingsOption | TwoFactorSettingsOption[];
}

interface TwoFactorSettingsOption {
  [key: string]: number;
}

export interface Enable2Fa {
  email?: string;
  action: string;
  type: string;
  options: TwoFactorSettingsOption | TwoFactorSettingsOption[];
}

@Injectable({
  providedIn: 'root'
})
export class TwoFactorService {
  public static TwoFactors = [];

  private baseUrl = environment.baseUrl;

  constructor(private http: HttpClient, private store: Store) {}

  getAllTwoFactor(): Observable<TwoFactorMethods[]> {
    return this.http
      .get<TwoFactorMethods[]>(`${this.baseUrl}/twofactor/getAllTwoFactor`)
      .pipe(first());
  }

  getAllActions(): Observable<TwoFactorAction[]> {
    return this.http
      .get<TwoFactorAction[]>(`${this.baseUrl}/twofactor/getAllActions`)
      .pipe(first());
  }

  updateAction(type: string, status: number) {
    return this.http
      .put(`${this.baseUrl}/twofactor/updateAction`, {
        type,
        status
      })
      .pipe(first());
  }

  getUserSettings(): Observable<TwoFactorSettings[]> {
    return this.http
      .get<TwoFactorSettings[]>(
        `${this.baseUrl}/twofactor/getUserSettings/${this.userEmail}/`
      )
      .pipe(first());
  }

  updateOptions(options: TwoFactorSettingsOption[], id: number) {
    return this.http
      .put(`${this.baseUrl}/twofactor/updateSettings/${id}/`, options)
      .pipe(first());
  }

  off2fa(id: number) {
    return this.http
      .delete(`${this.baseUrl}/twofactor/removeSettings/${id}/`)
      .pipe(first());
  }

  acceptAction(id: number, code: any) {
    return this.http
      .post(`${this.baseUrl}/twofactor/acceptAction/${id}/${code}/`, {})
      .pipe(first());
  }

  on2fa(payload: Enable2Fa) {
    return this.http
      .post(`${this.baseUrl}/twofactor/enableAction/`, {
        ...payload,
        email: this.userEmail
      })
      .pipe(first());
  }

  private get userEmail() {
    return this.store.value[Section.user].email;
  }
}
