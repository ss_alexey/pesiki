import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map, tap, first } from 'rxjs/operators';
import {
  Country,
  UserProfile,
  OrderInterface,
  Market,
  Currency,
  Wallet,
  OrderTransactionInterface,
  MyHistoryOrder,
  MyOrderInterface
} from '../models';
import { Store, Section } from '../app/store';
import { environment } from '../environments/environment';
// import * as math from 'mathjs';

export enum OrderType {
  buy = 1,
  sell = 2
}

export interface CurrencyPair {
  currency: string;
  targetCurrency: string;
  id: number;
}

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  public static Massengers = [
    { label: 'Viber', name: 'viber', img: 'assets/img/messengers/viber.svg' },
    {
      label: 'Telegram',
      name: 'telegram',
      img: 'assets/img/messengers/telegram.svg'
    },
    {
      label: 'WhatsApp',
      name: 'whatsapp',
      img: 'assets/img/messengers/whatsapp.svg'
    },
    { label: 'Wichat', name: 'wichat', img: 'assets/img/messengers/wichat.svg' }
  ];

  // public static TranslateLangs = ['en', 'ch', 'ru'];
  public static TranslateLangs = ['en', 'ru'];

  // 'BTC': ['LTC', 'DOGE', 'STR', 'XPR', 'ETH'],
  //       'USDT': ['BTC', 'STR', 'DASH', 'XPR', 'ETH'],
  //       'ETH': ['OMG', 'ZEC', 'LSK', 'STEEM', 'ETC']

  public static CurrencyPairs = {
    50: 'BTC_LTC',
    27: 'BTC_DOGE',
    89: 'BTC_STR',
    117: 'BTC_XRP',
    148: 'BTC_ETH',
    121: 'USDT_BTC',
    125: 'USDT_STR',
    122: 'USDT_DASH',
    126: 'USDT_XMR',
    149: 'USDT_ETH'
    //    197: 'ETH_OMG',
    //    179: 'ETH_ZEC',
    //    166: 'ETH_LSK',
    //    169: 'ETH_STEEM',
    //    172: 'ETH_ETC'
  };

  // public static CurrencyPairs = {
  //   7: 'BTC_BCN',
  //   14: 'BTC_BTS',
  //   15: 'BTC_BURST',
  //   20: 'BTC_CLAM',
  //   25: 'BTC_DGB',
  //   27: 'BTC_DOGE',
  //   24: 'BTC_DASH',
  //   38: 'BTC_GAME',
  //   43: 'BTC_HUC',
  //   50: 'BTC_LTC',
  //   51: 'BTC_MAID',
  //   58: 'BTC_OMNI',
  //   61: 'BTC_NAV',
  //   64: 'BTC_NMC',
  //   69: 'BTC_NXT',
  //   75: 'BTC_PPC',
  //   89: 'BTC_STR',
  //   92: 'BTC_SYS',
  //   97: 'BTC_VIA',
  //   100: 'BTC_VTC',
  //   108: 'BTC_XCP',
  //   114: 'BTC_XMR',
  //   116: 'BTC_XPM',
  //   117: 'BTC_XRP',
  //   112: 'BTC_XEM',
  //   148: 'BTC_ETH',
  //   150: 'BTC_SC',
  //   155: 'BTC_FCT',
  //   162: 'BTC_DCR',
  //   163: 'BTC_LSK',
  //   167: 'BTC_LBC',
  //   168: 'BTC_STEEM',
  //   170: 'BTC_SBD',
  //   171: 'BTC_ETC',
  //   174: 'BTC_REP',
  //   177: 'BTC_ARDR',
  //   178: 'BTC_ZEC',
  //   182: 'BTC_STRAT',
  //   184: 'BTC_PASC',
  //   185: 'BTC_GNT',
  //   189: 'BTC_BCH',
  //   192: 'BTC_ZRX',
  //   194: 'BTC_CVC',
  //   196: 'BTC_OMG',
  //   198: 'BTC_GAS',
  //   200: 'BTC_STORJ',
  //   201: 'BTC_EOS',
  //   204: 'BTC_SNT',
  //   207: 'BTC_KNC',
  //   210: 'BTC_BAT',
  //   213: 'BTC_LOOM',
  //   221: 'BTC_QTUM',
  //   229: 'BTC_MANA',
  //   121: 'USDT_BTC',
  //   216: 'USDT_DOGE',
  //   122: 'USDT_DASH',
  //   123: 'USDT_LTC',
  //   124: 'USDT_NXT',
  //   125: 'USDT_STR',
  //   126: 'USDT_XMR',
  //   127: 'USDT_XRP',
  //   149: 'USDT_ETH',
  //   219: 'USDT_SC',
  //   218: 'USDT_LSK',
  //   173: 'USDT_ETC',
  //   175: 'USDT_REP',
  //   180: 'USDT_ZEC',
  //   217: 'USDT_GNT',
  //   191: 'USDT_BCH',
  //   220: 'USDT_ZRX',
  //   203: 'USDT_EOS',
  //   206: 'USDT_SNT',
  //   209: 'USDT_KNC',
  //   212: 'USDT_BAT',
  //   215: 'USDT_LOOM',
  //   223: 'USDT_QTUM',
  //   231: 'USDT_MANA',
  //   129: 'XMR_BCN',
  //   132: 'XMR_DASH',
  //   137: 'XMR_LTC',
  //   138: 'XMR_MAID',
  //   140: 'XMR_NXT',
  //   181: 'XMR_ZEC',
  //   166: 'ETH_LSK',
  //   169: 'ETH_STEEM',
  //   172: 'ETH_ETC',
  //   176: 'ETH_REP',
  //   179: 'ETH_ZEC',
  //   186: 'ETH_GNT',
  //   190: 'ETH_BCH',
  //   193: 'ETH_ZRX',
  //   195: 'ETH_CVC',
  //   197: 'ETH_OMG',
  //   199: 'ETH_GAS',
  //   202: 'ETH_EOS',
  //   205: 'ETH_SNT',
  //   208: 'ETH_KNC',
  //   211: 'ETH_BAT',
  //   214: 'ETH_LOOM',
  //   222: 'ETH_QTUM',
  //   230: 'ETH_MANA',
  //   224: 'USDC_BTC',
  //   226: 'USDC_USDT',
  //   225: 'USDC_ETH',
  //   243: 'USDC_DOGE',
  //   244: 'USDC_LTC',
  //   242: 'USDC_STR',
  //   241: 'USDC_XMR',
  //   240: 'USDC_XRP',
  //   245: 'USDC_ZEC',
  //   235: 'USDC_BCH',
  //   237: 'USDC_BCHABC',
  //   239: 'USDC_BCHSV'
  // };

  baseUrl = environment.baseUrl;

  constructor(private http: HttpClient, private store: Store) {}
  // Countries
  public getCountries(): Observable<Country[]> {
    return this.http.get('https://restcountries.eu/rest/v2/all').pipe(
      map((countries: any[]) => {
        return countries.map(country => {
          const name = country.name;
          const nativeName = country.nativeName;
          const callingCode = '+' + country.callingCodes[0];
          const label = nativeName + ' (' + name + ')';
          const value = { name, nativeName, callingCode };

          return { label, value };
        });
      }),
      tap(countries => this.store.set(Section.countriesList, countries))
    );
  }

  // Cryptocurrency
  getMainCurrency() {
    const cryptocurrency = ['BTC', 'USDT', 'XMR', 'ETH', 'USDC'];
    return of(cryptocurrency);
  }

  // getCurrencyPairs(): { [id: number]: CurrencyPair[] } {
    // const pairs = ApiService.CurrencyPairs;
    // // convert {id: stringId, ... } --> {currency1: [{id, currency, targerCurrency}, {}...], currency2:[{}, {}] }
    // return Object.keys(pairs).reduce((prev, next) => {
    //   // const [first, second] = pairs[next].split('_');
    //   if (!prev[first]) {
    //     const item = { id: +next, currency: first, targetCurrency: second };
    //     return { ...prev, [first]: [item] };
    //   } else {
    //     const item = { id: +next, currency: first, targetCurrency: second };
    //     prev[first].push(item);
    //     return prev;
    //   }
    // }, {});
  // }

  // getFavoritePairs(): Observable<string[]> {
  //   return of([
  //     'ETH_BAT',
  //     'BTC_ETH',
  //     'BTC_SC',
  //     'BTC_CLAM',
  //     'BTC_XEM',
  //     'BTC_BCN',
  //     'ETH_LSK'
  //   ]).pipe(tap(data => this.store.set(Section.favoritePairs, data)));
  // }

  setFavorite(favorites: string[]) {
    localStorage.setItem('favoritePairs', JSON.stringify(favorites));
    this.store.set(Section.favoritePairs, favorites);
  }

  // validateSelectedPair(currency: string, targetCurrency: string): CurrencyPair {
  //   const pairs = this.getCurrencyPairs();
  //   const targetPair = pairs[currency].find(
  //     pair => pair.targetCurrency === targetCurrency
  //   );
  //   return targetPair ? targetPair : pairs[currency][0];
  // }

  getCurrencyPairId(par: { currency: string; targetCurrency: string }): number {
    const searchPair = `${par.currency}_${par.targetCurrency}`;
    const searchPair2 = `${par.targetCurrency}_${par.currency}`;
    const pairs = ApiService.CurrencyPairs;
    let id = Object.keys(pairs).find(key => pairs[key] === searchPair);
    if (!id) {
      id = Object.keys(pairs).find(key => pairs[key] === searchPair2);
    }
    return Number(id);
  }

  // USER:::
  getUserByEmail(email: string): Observable<UserProfile> {
    const userFromToken = this.store.value[Section.user];

    return this.http
      .get<UserProfile>(`${this.baseUrl}/user/${email}/`)
      .pipe(
        tap(user => this.store.set(Section.user, { ...userFromToken, ...user }))
      );
  }

  updateUser(
    email: string,
    userProfile: Partial<UserProfile>
  ): Observable<UserProfile> {
    const userFromToken = this.store.value[Section.user];

    return this.http
      .put<UserProfile>(`${this.baseUrl}/user/${email}/`, userProfile)
      .pipe(
        tap(user => this.store.set(Section.user, { ...userFromToken, ...user }))
      );
  }

  updateAvatar(email: string, avatar: any) {
    const user = this.store.value[Section.user];
    return this.http.post(`${this.baseUrl}/user/avatar/${email}/`, avatar).pipe(
      tap(avatarUrl =>
        this.store.set(Section.user, {
          ...user,
          avatarUrl: avatarUrl + '?' + Date.now()
        })
      )
    );
  }

  // END USER

  // ORDERS
  // createOrder(order: OrderInterface, option: number) {
  //   if (option === 1) {
  //     order.price = +math.round(1 / +order.price, 8);
  //     order.stopLoss = order.stopLoss
  //       ? +math.round(1 / +order.stopLoss, 8)
  //       : null;
  //     order.takeProfit = order.takeProfit
  //       ? +math.round(1 / +order.takeProfit, 8)
  //       : null;
  //     order.stop = order.stop ? +math.round(1 / +order.stop, 8) : null;
  //     order.limit = order.limit ? +math.round(1 / +order.limit, 8) : null;

  //     [order.currency, order.targetCurrency] = [
  //       order.targetCurrency,
  //       order.currency
  //     ];
  //   }

  //   return this.http.post(`${this.baseUrl}/order/${option}/`, order);
  // }

  getOrderById(orderId: string) {
    return this.http.get(`${this.baseUrl}/order/getOrder/${orderId}/`);
  }

  getOrderByUser(payload: {
    count?: number;
    margin?: number;
    status?: number;
    market?: string;
  }) {
    return this.http
      .post(`${this.baseUrl}/order/getOrderByUser/`, payload)
      .pipe(first());
  }

  getMyTradingHistory(count = 20) {
    // status 52 = 4 + 16 + 32
    const market = (this.store.value[
      Section.currencyPairSelected
    ] as CurrencyPair).currency;
    return this.getOrderByUser({ count, margin: 0, status: 52, market }).pipe(
      map((data: any) => {
        data.orders
          .map(order => new MyHistoryOrder())
          .sort(
            (order1: MyOrderInterface, order2: MyOrderInterface) =>
              +order2.CloseTime - +order1.CloseTime
          );

        return data;
      })
    );
  }

  getTransactionByOrderId(orderId): Observable<OrderTransactionInterface[]> {
    return this.http
      .get(`${this.baseUrl}/order/getTransactionByOrder/${orderId}/`)
      .pipe(
        map((data: { orders: OrderTransactionInterface[] }) => data.orders),
        first()
      );
  }

  updateOrder(payload, orderId) {
    return this.http.put(`${this.baseUrl}/order/${orderId}/`, payload);
  }

  cancelOrder(orderId: string) {
    return this.http.get(`${this.baseUrl}/order/CancelOrder/${orderId}/`);
  }

  // END ORDERS

  // WALLET
  getWallets(email: string): Observable<Wallet[]> {
    return this.http.get<Wallet[]>(`${this.baseUrl}/wallet/get/${email}/`);
  }

  getWalletByCurrency(email: string, currency: string): Observable<Wallet> {
    return this.http.get<Wallet>(
      `${this.baseUrl}/wallet/get/${email}/${currency}/`
    );
  }

  createWallet(email: string, currencyCode: string) {
    return this.http.post(`${this.baseUrl}/wallet/createWallet/`, {
      email,
      currencyCode
    });
  }

  createWithdraw(payload) {
    return this.http.post(`${this.baseUrl}/wallet/createWithdrawal/`, payload);
  }

  // END WALLET

  // MARKET
  getMarkets(): Observable<Market[]> {
    return this.http.get<Market[]>(`${this.baseUrl}/market/get/`);
  }

  getCurrencies(): Observable<Currency[]> {
    return this.http.get<Currency[]>(`${this.baseUrl}/market/currency/get/`);
  }

  // END MARKET
}
