import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Store, Section } from '../app/store';
import * as jwtDecode from 'jwt-decode';
// import {} from ''
import { environment } from '../environments/environment.prod';

export interface RegistrationUser {
  email: string;
  firstName?: string;
  lastName?: string;
  phoneCode: string;
  phoneNumber: string;
  country: string;
  city: string;
  code: string;
  password: string;
}

export interface Phone {
  phoneCode: string;
  phoneNumber: string;
}

export interface CheckExist {
  isExist: boolean;
  message: boolean;
}

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
  private baseUrl = environment.baseUrl + '/auth';

  constructor(private http: HttpClient, private store: Store) {}

  login(email: string, password: string, code: string) {
    return this.http
      .post<any>(`${this.baseUrl}/Login/`, { email, password, code })
      .pipe(
        map(user => {
          // login successful if there's a jwt token in the response
          if (user && user.token) {
            this.refreshUser(user);
          }
          return user;
        })
      );
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('cabinetCurrentUser');
    this.store.set(Section.user, null);
  }

  private refreshUser(user) {
    // store user details and jwt token in local storage to keep user logged in between page refreshes
    localStorage.setItem('cabinetCurrentUser', JSON.stringify(user));

    const userDecoded = jwtDecode(localStorage.getItem('cabinetCurrentUser'));
    this.store.set(Section.user, userDecoded);
  }

  refreshToken(token) {
    const currentUser = JSON.parse(localStorage.getItem('cabinetCurrentUser'));
    if (currentUser) {
      currentUser['token'] = token;
      localStorage.setItem('cabinetCurrentUser', JSON.stringify(currentUser));

      // Updating Store
      const userDecoded = jwtDecode(localStorage.getItem('cabinetCurrentUser'));
      this.store.set(Section.user, userDecoded);
    }
  }

  resetPassword(parameters: any) {
    return this.http.post<any>(this.baseUrl + '/resetPassword/', parameters);
  }

  updatePassword(parameters: any) {
    return this.http.post<any>(
      this.baseUrl + '/acceptResetPassword/',
      parameters
    );
  }

  createUser(parameters: RegistrationUser): Observable<any> {
    return this.http.post(this.baseUrl + '/registration/', parameters);
  }

  acceptPhoneCode(parameters: Phone): Observable<any> {
    return this.http.post(this.baseUrl + '/acceptPhone/', parameters);
  }

  checkPhoneExist(phone: Phone) {
    return this.http.post(this.baseUrl + '/checkPhone/', phone);
  }

  checkEmailExist(email: string) {
    return this.http.post(this.baseUrl + '/checkEmail/', { email });
  }

  isMob() {
    return true;
  }

}
