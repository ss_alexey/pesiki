import { Injectable } from '@angular/core';
import { timer } from 'rxjs';
import { map, take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TimerService {
  constructor() {}

  startTimer(sec = 60) {
    return timer(0, 1000).pipe(
      take(sec + 1),
      map(tick => sec - tick),
    );
  }
}
