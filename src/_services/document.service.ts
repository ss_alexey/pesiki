import { Injectable } from '@angular/core';
import { throwError, Observable } from 'rxjs';
import { Store, Section } from '../app/store';
import { environment } from '../environments/environment';
import { HttpClient } from '@angular/common/http';
import { switchMap, map, first } from 'rxjs/operators';

export interface DocumentTypes {
  name: string;
  description: string;
  status: number;
}

export interface DocumentUserDetails {
  name: string;
  type: string;
  guid: string;
  url: string;
  status: number;
  comment: null;
  uploadDate: string;
}

export interface Base64 {
  base64: string;
  mimeType?: string;
  name?: string;
}

@Injectable({
  providedIn: 'root'
})
export class DocumentService {
  constructor(private store: Store, private httpService: HttpClient) {}

  private convertDocumentTo64(
    file: File,
    documentType: DocumentType
  ): Observable<any> {
    let msg = '';

    const acceptFileTypes = [
      'image/png',
      'image/jpg',
      'image/jpeg',
      'application/pdf'
    ];

    if (1024 * 1024 * 2 < file.size) {
      msg = 'File should be less than 2Mb';
      return throwError(msg);
    }

    if (!acceptFileTypes.includes(file.type)) {
      msg = 'File type Error! You can upload .png, .jpg or .pdf files';
      return throwError(msg);
    }

    const reader = new FileReader();
    reader.readAsDataURL(file);

    return new Observable(observer => {
      reader.onerror = err => observer.error(err);
      reader.onabort = err => observer.error(err);

      reader.onload = () => {
        const document = reader.result;
        const data = document.toString().replace(/^data:\w+\/\w+;base64,/, '');
        const params = {
          data,
          name: file.name,
          type: documentType,
          email: this.email
        };

        observer.next(params);
      };

      reader.onloadend = () => observer.complete();
    });
  }

  readAvatar(file: File): Observable<any> {
    let msg = '';
    const acceptFileTypes = ['image/png', 'image/jpg', 'image/jpeg'];

    if (2 * 1024 * 1024 < file.size) {
      msg = 'File should be less than 2Mb';
      return throwError(msg);
    }

    if (!acceptFileTypes.includes(file.type)) {
      msg = 'File type Error! You can upload .png, or .jpg';
      return throwError(msg);
    }

    const reader = new FileReader();
    reader.readAsDataURL(file);

    return new Observable(observer => {
      reader.onerror = err => observer.error(err);
      reader.onabort = err => observer.error(err);

      reader.onload = () => {
        const document = reader.result;
        const avatarBase64 = document
          .toString()
          .replace(/^data:\w+\/\w+;base64,/, '');
        const params = {
          avatarBase64,
          mimeType: file.type
        };

        observer.next(params);
      };

      reader.onloadend = () => observer.complete();
    });
  }

  convertToBase64(
    file: File,
    types: string[] = ['image/png', 'image/jpg', 'image/jpeg'],
    maxSize = 2 * 1024 * 1024
  ): Observable<Base64> {
    let msg = {};

    if (maxSize < file.size) {
      msg = {
        message: 'limitFileSize',
        limits: maxSize / (1024 * 1024),
        fileName: file.name
      };
      return throwError(msg);
    }

    if (!types.includes(file.type)) {
      msg = {
        message: 'limitTypes',
        limits: types.join(', '),
        fileName: file.name
      };
      return throwError(msg);
    }

    const reader = new FileReader();
    reader.readAsDataURL(file);

    return new Observable(observer => {
      reader.onerror = err => observer.error(err);
      reader.onabort = err => observer.error(err);

      reader.onload = () => {
        const document = reader.result;
        const base64 = document
          .toString()
          .replace(/^data:\w+\/\w+;base64,/, '');
        const params = {
          base64,
          mimeType: file.type,
          name: file.name
        };

        observer.next(params);
      };

      reader.onloadend = () => observer.complete();
    });
  }

  getDocumentsWithSettings() {
    let documents;
    return this.getDocuments().pipe(
      switchMap(data => {
        documents = data;
        return this.getUserDocuments();
      }),
      map(userDocuments => {
        return documents.map(doc => {
          const userDocDetails = this.getDocumentTypeDetails(
            doc.name,
            userDocuments
          );
          return userDocDetails ? { ...doc, user: userDocDetails } : { ...doc };
        });
      }),
      first()
    );
  }

  uploadDocument(file: File, documentType: DocumentType) {
    return this.convertDocumentTo64(file, documentType).pipe(
      switchMap(payload => this.createDocument(payload)),
      first()
    );
  }

  private createDocument(payload: any) {
    return this.httpService.post(
      `${environment.baseUrl}/document/uploadUserDoc/`,
      payload
    );
  }

  private getUserDocuments(): Observable<DocumentUserDetails[]> {
    return this.httpService.get<DocumentUserDetails[]>(
      `${environment.baseUrl}/document/get/${this.email}/`
    );
  }

  private getDocuments(): Observable<DocumentTypes[]> {
    return this.httpService.get<DocumentTypes[]>(
      `${environment.baseUrl}/document/get/`
    );
  }

  private get email() {
    return this.store.value[Section.user]['email'];
  }

  private getDocumentTypeDetails(type: string, arr: DocumentUserDetails[]) {
    return arr.find(doc => doc.type === type) || null;
  }
}
