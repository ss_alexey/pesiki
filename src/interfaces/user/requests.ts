export interface LoginUser {
    uid: string;
    password: string;
}
export interface RegisterUser {
    email: string;
    phoneNumber?: string;
    username: string;
    password: string;
}
export interface UpdateUserParamsRequest {
    avatar: number;
    username: string;
    phoneNumber: string;
}

