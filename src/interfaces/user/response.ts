
export interface LoginResponse {
    AvatarUrl: number;
    Balance_LTC: number;
    CDS: number;
    EXP: number;
    Earned_LTC: number;
    Email: string;
    ID: number;
    OwnedDogs: any[];
    PASS_HASH: string;
    Phone: string;
    Rank: string;
    Referred_By: null | any;
    Status: number;
    Username: string;
    achievements: null;
    Token: string;
}
export type RegisterResponse = LoginResponse;
// export type RegisterResponse = [{
//     ID_USER: number;
//     USERNAME: string;
//     Balance_LTC: 0;
//     Earned_LTC: 0;
//     Address_Out_LTC: string;
//     Address_Out_LTC_P: string;
//     Address_Out_LTC_External: null;
//     Address_Out_LTC_Internal: string;
//     EMAIL: string;
//     PHONE: string;
//     PASS_HASH: string;
//     EXP: number;
//     BIN: string;
//     email_when_NEW_PUPPY: true;
//     email_when_IGETPAID: true;
//     email_when_SPECIAL_EVENT: true;
//     text_when_NEW_PUPPY: true;
//     text_when_IGETPAID: true;
//     text_when_SPECIAL_EVENT: true;
//     Phone_verified: false;
//     Status: 1;
//     membership: null;
//     country: string;
//     email_verified: false;
//     lastlogin_time: string;
//     ip_address: string;
//     WALET_UPDATED: false;
//     avatar: number;
//     CDS: number;
//     Referred_By: number;
//     SEC_HASH: number;
//     ReferrerDomain: null | any;
// }];

