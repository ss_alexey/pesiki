

export interface UserParams {
    AvatarUrl: string;
    Balance_LTC: number;
    CDS: number;
    EXP: number;
    Earned_LTC: number;
    Email: string;
    ID: number;
    OwnedDogs: any[];
    PASS_HASH: string;
    Phone: string;
    Rank: string;
    Referred_By: null | any;
    Status: number;
    Username: string;
    achievements: null;
}

