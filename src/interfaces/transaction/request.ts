export interface TransactionPostRequest {
    addressFrom: string;
    addressTo: string;
    amount: number;
    currency: string;
    cdsToken: number;
}
