export type TransactionGetResponse = [{
    user_id: number;
    addressFrom: string;
    addressTo: string;
    date: string;
    currency: string;
    amount: number;
    cdsToken: number;
    status: number;
}];

