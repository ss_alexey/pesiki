export interface Market {
  masterCurrecyCode: string;
  targetCurrencies: Currency[];
  status: number;
}

export interface Currency {
  name: string;
  code: string;
  status: number;
  isFixedFee: boolean;
  fixedFee: number;
  minFee: number;
  maxFee: number;
}
