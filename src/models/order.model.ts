// import * as math from 'mathjs';

export interface OrderInterface {
  currency: string;
  targetCurrency: string;
  price: number;
  amount: number;
  userId: string;
  expiresTime?: Date;
  stopLoss?: number;
  takeProfit?: number;
  stop?: number;
  limit?: number;
  twoFactorCode?: string;
}

export interface MyOrderDetails {
  orderId: number;
  currency: string;
  targetCurrency: string;
  expiresTime: Date;
  price: number;
  amount: number;
  takeProfit: number;
  stopLoss: number;
  stop: number;
  limit: number;
  orderType: number;
  comment: string;

  // creationTime?: Date | null;
  // closedTime?: Date | null;
  // commision?: number;
  // clerSum?: number;
  // summ?: number;
}

export class Order implements OrderInterface {
  currency: string;
  targetCurrency: string;
  price: number;
  amount: number;
  userId: string;
  expiresTime?: Date;
  stopLoss?: number;
  takeProfit?: number;
  stop?: number;
  limit?: number;

  constructor(order: OrderInterface) {
    this.currency = order.currency;
    this.targetCurrency = order.targetCurrency;
    this.price = +order.price;
    this.amount = +order.amount;
    this.takeProfit = order.takeProfit ? +order.takeProfit : null;
    this.stopLoss = order.stopLoss ? +order.stopLoss : null;
    this.stop = order.stop ? +order.stop : null;
    this.limit = order.limit ? +order.limit : null;
    this.expiresTime = order.expiresTime ? new Date(order.expiresTime) : null;
  }
}

export interface BookOrderInterface {
  type: 'sell' | 'buy';
  price: number | string;
  currencyValue: number | string;
  targetCurrencyValue: number | string;
  amount: number | string;
  sum: number | string;
}

// export interface TradeOrderInterface {
//   orderId: number;
//   isMyOrder: boolean;
//   currency: string;
//   targetCurrency: string;
//   type: 'sell' | 'buy';
//   createDate: Date;
//   openDate: Date;
//   closeDate: Date;
//   price: number;
//   volume: number;
//   summ: number;
//   stopLoss: number | null;
//   takeProfit: number | null;
//   stop: number | null;
//   limit: number | null;
//   commission: number;
//   clearSumm: number;
//   expiresTime: Date;
//   comment: string | null;
//   transaction: any[]; // type details will be later
//   status: string;
// }

// export interface TradeDate {
//   date: Date;
//   quotes: [{ key: string; value: number }];
//   bookOrders: BookOrderInterface[];
//   orders: TradeOrderInterface[]; //will be myOrders, Pending Orders, History, MyHistory
// }

export interface AgregatedOrder {
  Price: number | string;
  Pair: string;
  Amount: number | string;
  Sum: number | string;
  TotalAmount: number | string;
  TotalSum: number | string;
}

export interface MyOrderInterface {
  ClearSum: number;
  Commision: number;
  CreationTime: Date;
  GoodTillDate: Date;
  Id: number;
  Pair: string;
  Price: number;
  Status: number;
  StopLoss: number;
  Sum: number;
  TakeProfit: number;
  Type: number;
  Volume: number;
  Comment?: string;
  AvarageCost: number;
  CloseTime?: Date | null;
}

export class MyOrder implements MyOrderInterface {
  ClearSum: number;
  Commision: number;
  CreationTime: Date;
  GoodTillDate: Date;
  Id: number;
  Pair: string;
  Price: number;
  Status: number;
  StopLoss: number;
  Sum: number;
  TakeProfit: number;
  Type: number;
  Volume: number;
  Comment?: string;
  AvarageCost: number;
  CloseTime?: Date;
  constructor() { }
  // constructor(order: MyOrderInterface) {
  //   this.Status = +order.Status;
  //   this.GoodTillDate = order.GoodTillDate
  //     ? new Date(order.GoodTillDate)
  //     : null;
  //   this.CloseTime = order.CloseTime ? new Date(order.CloseTime) : null;
  //   this.CreationTime = order.CreationTime
  //     ? new Date(order.CreationTime)
  //     : null;

  //   this.Id = +order.Id;
  //   this.Type = +order.Type;
  //   this.Comment = order.Comment || null;
  //   this.Volume = +math.round(+order.Volume, 8);

  //   if (order.Type === 1) {
  //     this.Commision = +order.Sum
  //       ? +math.round(+order.Commision / +order.Sum, 10)
  //       : 0;
  //     this.Price = order.Price ? +math.round(1 / +order.Price, 8) : 0;
  //     this.StopLoss = order.StopLoss ? +math.round(1 / +order.StopLoss, 8) : 0;
  //     this.AvarageCost = +order.AvarageCost
  //       ? +math.round(1 / +order.AvarageCost, 8)
  //       : 0;
  //     this.TakeProfit = order.TakeProfit
  //       ? +math.round(1 / +order.TakeProfit, 8)
  //       : 0;
  //     this.Sum = +math.round(this.Volume * this.Price, 8);
  //     this.Commision = +math.round(this.Commision * this.Sum, 8);
  //     this.ClearSum = +math.round(+this.Sum - this.Commision, 8);
  //     this.Pair = order.Pair.replace('_', ' / ');
  //   } else {
  //     this.ClearSum = +math.round(+order.ClearSum, 8);
  //     this.Commision = +math.round(+order.Commision, 8);
  //     this.Price = +math.round(+order.Price, 8);
  //     this.StopLoss = +math.round(+order.StopLoss, 8);
  //     this.Sum = +math.round(+order.Sum, 8);
  //     this.TakeProfit = +math.round(+order.TakeProfit, 8);
  //     this.AvarageCost = this.AvarageCost
  //       ? +math.round(+order.AvarageCost, 8)
  //       : 0;

  //     this.Pair = order.Pair.split('_')
  //       .reverse()
  //       .join(' / ');
  //   }
  // }
}

export class MyHistoryOrder implements MyOrderInterface {
  ClearSum: number;
  Commision: number;
  CreationTime: Date;
  GoodTillDate: Date;
  Id: number;
  Pair: string;
  Price: number;
  Status: number;
  StopLoss: number;
  Sum: number;
  TakeProfit: number;
  Type: number;
  Volume: number;
  Comment?: string;
  AvarageCost: number;
  CloseTime?: Date;
  constructor() { }
  // constructor(order: MyOrderInterface) {
  //   this.Status = +order.Status;
  //   this.GoodTillDate = order.GoodTillDate
  //     ? new Date(order.GoodTillDate)
  //     : null;
  //   this.CloseTime = order.CloseTime ? new Date(order.CloseTime) : null;
  //   this.CreationTime = order.CreationTime
  //     ? new Date(order.CreationTime)
  //     : null;

  //   this.Id = +order.Id;
  //   this.Type = +order.Type;
  //   this.Comment = order.Comment || null;

  //   // Got comission %
  //   order.Commision = +order.Sum
  //     ? +math.round(+order.Commision / +order.Sum, 10)
  //     : 0;
  //   this.Volume = +math.round(+order.Volume, 8);

  //   if (order.Type === 1) {
  //     this.Price = order.Price ? +math.round(1 / +order.Price, 8) : 0;
  //     this.StopLoss = order.StopLoss ? +math.round(1 / +order.StopLoss, 8) : 0;
  //     this.TakeProfit = order.TakeProfit
  //       ? +math.round(1 / +order.TakeProfit, 8)
  //       : 0;
  //     this.AvarageCost = order.AvarageCost
  //       ? +math.round(1 / +order.AvarageCost, 8)
  //       : 0;
  //     this.Pair = order.Pair.replace('_', ' / ');
  //   } else {
  //     this.Price = +math.round(+order.Price, 8);
  //     this.StopLoss = +math.round(+order.StopLoss, 8);
  //     this.TakeProfit = +math.round(+order.TakeProfit, 8);
  //     this.AvarageCost = +math.round(+order.AvarageCost, 8);
  //     this.Pair = order.Pair.split('_')
  //       .reverse()
  //       .join(' / ');
  //   }

  //   this.Sum = +math.round(this.AvarageCost * this.Volume, 8);
  //   this.Commision = +math.round(+order.Commision * this.Sum, 8);
  //   this.ClearSum = +math.round(this.Sum - this.Commision, 8);
  // }
}
export interface UpdateMyOrderInterface {
  expiresTime: Date;
  price: number;
  amount: number;
  stopLoss: number;
  takeProfit: number;
  stop: number;
  limit: number;
  comment: string;
}

export class UpdateMyOrder implements UpdateMyOrderInterface {
  expiresTime: Date;
  price: number;
  amount: number;
  stopLoss: number;
  takeProfit: number;
  stop: number;
  limit: number;
  comment: string;
  constructor() { }
  // constructor(order: MyOrderDetails) {
  //   this.expiresTime = order.expiresTime ? new Date(order.expiresTime) : null;
  //   this.comment = order.comment;
  //   this.amount = +math.round(+order.amount, 8);

  //   if (order.orderType === 1) {
  //     this.price = this.price ? +math.round(1 / +order.price, 8) : this.price;
  //     this.stopLoss = this.stopLoss
  //       ? +math.round(1 / +order.stopLoss, 8)
  //       : this.stopLoss;
  //     this.takeProfit = this.takeProfit
  //       ? +math.round(1 / +order.takeProfit, 8)
  //       : this.takeProfit;
  //     this.stop = this.stop ? +math.round(1 / +order.stop, 8) : this.stop;
  //     this.limit = this.limit ? +math.round(1 / +order.limit, 8) : this.limit;
  //   } else {
  //     this.price = +math.round(+order.price, 8);
  //     this.stopLoss = +math.round(+order.stopLoss, 8);
  //     this.takeProfit = +math.round(+order.takeProfit, 8);
  //     this.stop = +math.round(+order.stop, 8);
  //     this.limit = +math.round(+order.limit, 8);
  //   }
  // }
}

export interface OrderTransactionInterface {
  amount: number;
  clearSum: number;
  commission: number;
  dateTime: Date | null;
  id: number;
  price: number;
  sum: number;
}

export class OrderTransaction implements OrderTransactionInterface {
  amount: number;
  clearSum: number;
  commission: number;
  dateTime: Date | null;
  id: number;
  price: number;
  sum: number;
  constructor() { }
  // constructor(transaction: OrderTransactionInterface, reverse = false) {
  //   this.amount = +math.round(+transaction.amount, 8);
  //   this.id = +transaction.id;
  //   this.dateTime = transaction.dateTime
  //     ? new Date(transaction.dateTime)
  //     : null;

  //   if (reverse) {
  //     // Get % of comission
  //     transaction.commission = transaction.sum
  //       ? +transaction.commission / +transaction.sum
  //       : 0;

  //     this.price = transaction.price
  //       ? +math.round(1 / +transaction.price, 8)
  //       : 0;
  //     this.sum = +math.round(this.price * this.amount, 8);

  //     this.commission = +math.round(+transaction.commission * this.sum, 8);
  //     this.clearSum = +math.round(this.sum - this.commission, 8);
  //   } else {
  //     this.commission = +math.round(+transaction.commission, 8);
  //     this.price = +math.round(+transaction.price, 8);
  //     this.sum = +math.round(+transaction.sum, 8);
  //     this.clearSum = +math.round(+transaction.clearSum, 8);
  //   }
  // }
}
