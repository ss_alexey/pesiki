export interface ViewOptions {
  myTradingHistory: boolean;
  tradingHistory: boolean;
  buyOrders: boolean;
  sellOrders: boolean;
  favoriteCoins: boolean;
  hotCoins: boolean;
  aboveTheMenu: boolean;
  overChart: boolean;
}
