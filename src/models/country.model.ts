export interface Country {
  label: string;
  value: {
    nativeName: string;
    name: string;
    callingCode: string;
  };
}
