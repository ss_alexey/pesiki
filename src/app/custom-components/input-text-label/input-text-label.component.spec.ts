import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InputTextLabelComponent } from './input-text-label.component';

describe('InputTextLabelComponent', () => {
  let component: InputTextLabelComponent;
  let fixture: ComponentFixture<InputTextLabelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InputTextLabelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputTextLabelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
