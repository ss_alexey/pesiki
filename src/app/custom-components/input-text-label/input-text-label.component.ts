import { Component, OnInit, forwardRef, Optional, Attribute, ViewChild, ElementRef, Host, SkipSelf, Input, HostListener } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, ControlContainer, AbstractControl } from '@angular/forms';
import { InputTextComponent } from '../input-text/input-text.component';

@Component({
  selector: 'app-input-text-label',
  templateUrl: './input-text-label.component.html',
  styleUrls: ['./input-text-label.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => InputTextLabelComponent),
    }
  ],
})
export class InputTextLabelComponent implements ControlValueAccessor, OnInit {
  hover = false;
  active = false;
  value = '';
  control: AbstractControl;
  formControlName: string;
  placeholder: string;
  @Input()
  labelBefore?: boolean;
  @Input()
  labelAfter?: boolean;
  @Input()
  svgImg?: string;
  @ViewChild('input')
  input: ElementRef;
  @HostListener('document:click', ['$event'])
  clickout(event) {
    if (this.eRef.nativeElement.contains(event.target)) {


    } else {
      this.active = false;
      this.hover = false;
      this.input.nativeElement.blur();
    }
  }




  constructor(
    private eRef: ElementRef,
    @Optional() @Host() @SkipSelf()
    private controlContainer: ControlContainer,
    @Attribute('formControlName') controlName,
    @Attribute('placeholder') placeholder,

  ) {
    this.formControlName = controlName;
    this.placeholder = placeholder;
    console.log(this.placeholder);


  }

  onChange: (_: any) => void = (_: any) => { };


  onTouched: () => void = () => { };
  deligerToInput = (e: Event) => {
    // this.input.nativeElement.focus();

  }
  checkErrors = () => {
    // if (!this.control.value) {
    //   this.transformPlaceholder = false;
    // }
    // if (this.control.errors) {
    //   this.errors = this.control.errors;
    //   this.showError = !!this.errors;
    // }
    // if (!this.errors) {
    //   this.showError = false;
    // }

  }
  updateChanges() {
    this.onChange(this.value);
    // this.errors = this.control.errors;
    // if (this.control.valid) {
    //   this.showError = false;
    // }



    // if (!!this.control.value) {
    //   this.transformPlaceholder = true;
    // }
    // if (this.transformPlaceholder && !this.control.value && !this.focus) {
    //   this.transformPlaceholder = false;
    // }
  }
  changeTranformPlaceholder = (value: boolean) => {
    // this.transformPlaceholder = value;
  }

  writeValue(value: any): void {
    this.value = value;
    this.updateChanges();

  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }


  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }
  setDisabledState(isDisabled: boolean): void {
    // this.disabled = isDisabled;
  }
  getError = () => {
    return 'error';
  }
  ngOnInit() {


    if (this.controlContainer) {
      if (this.formControlName) {
        this.control = this.controlContainer.control.get(this.formControlName);
      } else {
        console.warn('Missing FormControlName directive from host element of the component');
      }
    } else {
      console.warn('Can\'t find parent FormGroup directive');
    }

  }
  test() {
    console.log('HEREW');
    this.active = true;
    this.input.nativeElement.focus()

  }
  mouseLeave() {
    if (!this.active) {
      this.hover = false;
    }
  }
}
