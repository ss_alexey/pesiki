import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MobileService {
  isMobile = false;
  setIsMobile(value: boolean) {
    this.isMobile = value;
  }
}
