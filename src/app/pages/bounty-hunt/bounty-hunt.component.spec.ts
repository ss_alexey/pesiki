import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BountyHuntComponent } from './bounty-hunt.component';

describe('BountyHuntComponent', () => {
  let component: BountyHuntComponent;
  let fixture: ComponentFixture<BountyHuntComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BountyHuntComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BountyHuntComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
