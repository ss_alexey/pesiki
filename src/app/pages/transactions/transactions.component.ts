import { Component, OnInit, OnDestroy, AfterContentInit, AfterViewInit } from '@angular/core';

import { TransactionService, QrCodeService } from '@services';
import { TransactionParams } from '@interfaces';

import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { style } from '@angular/animations';
import { environment } from '@/environments/environment.prod';




@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.scss']
})
export class TransactionsComponent implements OnInit, OnDestroy, AfterViewInit {
  IE: boolean;
  transactions: TransactionParams[] = [];
  transactionsUpdated = false;
  sortArrayBy: string;
  sortArrayReverse: boolean;
  constructor(
    private transactionsService: TransactionService,
    private qrService: QrCodeService,
    private router: Router
  ) { }
  ngAfterViewInit() {
    // const grid = document.getElementById('grid');

    // grid.getElementsByTagName('div').item(1).style['ms-grid-column'] = 2;
    // grid.getElementsByTagName('div').item(1)
    // console.log(grid.getElementsByTagName('div').item(1).style);
  }
  ngOnInit() {



    this.updateTransactions();

  }
  sortArray = (array: any[], sortBy: string, reverse: boolean): any[] => {
    let sortType;
    let sortedArray: any[];
    if (!isNaN(+array[0][sortBy])) {
      if (!isNaN(new Date(array[0][sortBy]).getTime()) && typeof array[0][sortBy] !== 'number') {
        sortType = 'DATE';
      } else {
        sortType = 'NUMBER';
      }

    } else {
      sortType = 'STRING';
    }
    if (sortType === 'DATE') {
      sortedArray = array.sort((item1, item2) => {
        const date1 = (new Date(item1[sortBy])).getTime();
        const date2 = (new Date(item2[sortBy])).getTime();
        if (!reverse) {
          if (date1 < date2) {
            return 1;
          } else {
            return -1;
          }
        } else {
          if (date1 > date2) {
            return 1;
          } else {
            return -1;
          }
        }

      });
    } else {
      sortedArray = array.sort((item1, item2) => {
        if (!reverse) {
          if (item1[sortBy] < item2[sortBy]) {
            return 1;
          } else {
            return -1;
          }
        } else {
          if (item1[sortBy] > item2[sortBy]) {
            return 1;
          } else {
            return -1;
          }
        }

      });
    }
    return sortedArray;
  }
  sortArrayHandler = (sortBy: string) => {
    if (this.sortArrayBy !== sortBy) {
      this.sortArrayBy = sortBy;
    } else {
      this.sortArrayReverse = !this.sortArrayReverse;
    }
    this.transactions = this.sortArray(this.transactions, sortBy, this.sortArrayReverse);

  }
  updateTransactions() {
    if (!this.transactionsUpdated) {
      this.transactionsService.getTransactions().toPromise().then((res: TransactionParams[]) => {
        let sorted: any[];
        if (this.sortArrayBy) {
          sorted = this.sortArray(res, 'date', this.sortArrayReverse);
        } else {
          sorted = this.sortArray(res, this.sortArrayBy, this.sortArrayReverse);
        }

        this.transactionsUpdated = true;
        setTimeout(() => {
          this.transactionsUpdated = false;
        }, 5000);
        if (this.router.url === '/transactions') {
          this.transactions = sorted;
        } else {
          this.transactions = sorted.slice(0, 3);
        }
      }).catch(e => {
        console.log(e);

        // console.log('REMOVE THIS FROM HERE!!!!!!!');

        // if (e.status === 0 && environment.production) {
        //   this.transactions = new Array(3).fill({
        //     user_id: 123,
        //     addressFrom: 'string',
        //     addressTo: 'string',
        //     date: Date.now(),
        //     currency: 'FAKE',
        //     amount: 1.11111111111111111111111,
        //     cdsToken: 5,
        //     status: 2
        //   });
        // }
        // console.log(this.transactions);

      });
    }

  }
  getMarkColor = (status: number) => {
    switch (status) {
      case 2:
        return 'green';
      case 3:
        return 'red';
      default:
        return '#cccc00';

    }

  }
  getTooltiText = (status: number) => {
    switch (status) {
      case 2:
        return 'Already paid';
      case 3:
        return 'No payment made after 12 hours';
      default:
        return 'In process';

    }
  }
  navigate = (transaction: TransactionParams) => {
    if (transaction.status === 1) {
      this.qrService.setPayment(transaction);
      this.router.navigateByUrl('payment-detail');
      console.log(transaction);
    }
  }
  isIE() {
    const ua = navigator.userAgent;
    const Ie = ua.indexOf('MSIE ') > -1 || ua.indexOf('Trident/') > -1;
    this.IE = Ie;
    return Ie;
  }
  ngOnDestroy() {
  }
}
