import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ERC20Component } from './erc20.component';

describe('ERC20Component', () => {
  let component: ERC20Component;
  let fixture: ComponentFixture<ERC20Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ERC20Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ERC20Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
