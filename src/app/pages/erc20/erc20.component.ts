import { Component, OnInit } from '@angular/core';

import { WalletService } from '@services';

import { currencyWallet } from '@config';

@Component({
  selector: 'app-erc20',
  templateUrl: './erc20.component.html',
  styleUrls: ['./erc20.component.scss']
})
export class ERC20Component implements OnInit {
  erc20WalletAdress = `https://www.blockchain.com/btc/address/${currencyWallet.ETH}`;
  wallet = '0xC1c11321526D00912E5c7D159897214d7Fc69a91';
  copied: boolean;
  constructor(private walletService: WalletService) { }

  ngOnInit() {
    // const wallet = '';

    // const valid = this.walletService.validateWallet(wallet);
    // console.log(valid);
  }
  copy = () => {
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = this.wallet;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    this.copied = true;
  }
}
