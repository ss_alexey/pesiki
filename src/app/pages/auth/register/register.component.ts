import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService, FormValidatorService } from '@services';

import * as iso from 'i18n-iso-countries';

declare var require: any;

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})

export class RegisterComponent implements OnInit {
  states: any;
  @Output() toggleRegister = new EventEmitter<boolean>();
  form = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    phoneNumber: new FormControl(''),
    username: new FormControl('', [Validators.required, Validators.minLength(2)]),
    password: new FormControl('', [Validators.required, Validators.minLength(6)]),
    // firstName: new FormControl('', Validators.required),
    // lastName: new FormControl('', Validators.required),
    // email: new FormControl('', [Validators.required, Validators.email]),
    // password: new FormControl('', [Validators.required]),
    confirmPass: new FormControl('', [Validators.required, Validators.minLength(6)]),
    state: new FormControl(null)
    // country: new FormControl('', [Validators.required]),
    // city: new FormControl('', [Validators.required]),
    // phone: new FormControl('', [Validators.required])
  }, this.formValidatorService.validateIfSame('password', 'confirmPass'));
  constructor(private authService: AuthService, private formValidatorService: FormValidatorService) { }

  ngOnInit() {
    iso.registerLocale(require('i18n-iso-countries/langs/en.json'));
    this.states = Object.values(iso.getNames('en'));
    this.form.valueChanges.subscribe(res => {
      console.log(this.form);


    });
  }
  toggleToLogin() {
    this.toggleRegister.emit(false);
  }
  register() {

    this.authService.register(this.form.value).toPromise().then((res: any) => {
      // if (res && res.Token) {
      //   localStorage.setItem('token', res.Token);
      // }
      // this.userService.updateUser();
      this.toggleRegister.emit(false);
    }).catch(e => {

      console.log(e);
    });
  }
}

