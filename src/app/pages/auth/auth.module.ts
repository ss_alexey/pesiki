import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { MeetingComponent } from './meeting/meeting.component';
import { AuthComponent } from './auth.component';
import {AuthRoutingModule} from './auth-routing.module';
import {MatCheckboxModule} from '@angular/material/checkbox';


import {SharedModule} from '../../shared/shared.module';

@NgModule({
  declarations: [RegisterComponent, LoginComponent, MeetingComponent, AuthComponent],
  imports: [
    MatCheckboxModule,
    SharedModule,
    AuthRoutingModule,
    CommonModule
  ],
  exports: [AuthComponent]
})
export class AuthModule { }
