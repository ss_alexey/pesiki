import { Component, OnInit } from '@angular/core';
import { trigger, state, style, animate, transition, keyframes } from '@angular/animations';

import { CurrencyService } from '@services';

import { MobileService } from '../../mobile.service';


@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss'],
  animations: [
    trigger('easeInOut', [
      transition(':enter', [
        style({
          opacity: 0,
          // transform: 'scale(0)',
          width: '0%'
        }),
        animate('0.5s linear', style({
          opacity: 1,
          width: '100%',
          // transform: 'scale(1)'
        }))
      ]),
      transition(':leave', [
        style({
          opacity: 0,
          width: '100%',
          // transform: 'scale(1)'
        }),
        animate('0.5s linear', style({
          opacity: 1,
          width: '0%',
          // transform: 'scale(0)'
        }))
      ])
    ])
  ]
})
export class AuthComponent implements OnInit {
  displayLogin: boolean;
  mobile: boolean;
  constructor(private ismobile: MobileService, private service: CurrencyService) {
    this.mobile = ismobile.isMobile;
  }

  ngOnInit() {

  }
  toggle(displayLogin: boolean) {
    console.log(displayLogin);
    this.displayLogin = displayLogin;
    // this.displayLogin = displayLogin;
  }
}
