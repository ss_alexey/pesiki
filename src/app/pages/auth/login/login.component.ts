import { Component,  Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { AuthService, FormValidatorService } from '@services';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  @Output() toggleRegister = new EventEmitter<boolean>();

  form = new FormGroup({
    uid: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required]),
  });
  constructor(private authService: AuthService) {

  }

  signin() {
    // console.log(document.cookie);
    // this.setCookie();
    // if (!environment.production) {
    //   const user = {
    //     uid: 'test@test.com',
    //     password: 'testtest1234'
    //   };
    //   this.authService.login(user);
    //   return;
    // }
    this.authService.login(this.form.value);
  }
  // setCookie(cname?, cvalue?, exdays?) {
  //   // const d = new Date();
  //   // d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
  //   // const expires = 'expires=' + d.toUTCString();
  //   // document.cookie = cname + '=' + cvalue + '; ' + expires;
  //   // document.setCoo
  //   document.cookie = 'login=test@test.com; path=/; domain=.dev.coindogs.co; Expires=Thu, 03 Jun 2021 08:53:45 GMT;';
  //   console.log(document.cookie);

  // }
  toggleToRegister() {
    this.toggleRegister.emit(true);

  }

}
