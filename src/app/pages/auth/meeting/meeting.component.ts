import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
// import { MobileService } from '@app/services';

import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { config } from '../../../config';

@Component({
  selector: 'app-meeting',
  templateUrl: './meeting.component.html',
  styleUrls: ['./meeting.component.scss']
})
export class MeetingComponent implements OnInit {
  displayLogin = false;
  mobile = true;
  @Output() toogle = new EventEmitter<any>();
  constructor(private breakpointObserver: BreakpointObserver) {
    // this.mobile = ismobile.isMobile;
  }
  @Input()
  // displayLogin: boolean;
  ngOnInit() {
    // const isSmallScreen = this.breakpointObserver.isMatched('(max-width: 599px)');
    // BreakpointObserver.
    // const layoutChanges = this.breakpointObserver.observe([
    //   '(orientation: portrait)',
    //   '(orientation: landscape)',
    // ]);

    // layoutChanges.subscribe(result => {
    //   // updateMyLayoutForOrientationChange();
    //   console.log(result);

    // });
    this.breakpointObserver.observe([
      config.mobileWidth, config.tabletWidth
    ]).subscribe(result => {
      if (result.matches) {
        if (result.breakpoints[config.mobileWidth]) {
          console.log('mobile');
          return;
        }
        if (result.breakpoints[config.tabletWidth]) {
          console.log('tablet');
          return;
        }
      } else {
        console.log('BIG');

      }
    });
  }
  click() {
    this.displayLogin = !this.displayLogin;
    this.toogle.emit(this.displayLogin);
  }
}
