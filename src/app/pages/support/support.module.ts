import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../shared/shared.module';

import { SupportRoutingModule } from './support-routing.module';
import { SupportComponent } from './support.component';
import { FAQComponent } from './faq/faq.component';
import { CreateRequestComponent } from './create-request/create-request.component';
import { OldRequestComponent } from './old-request/old-request.component';
import { SupportChatComponent } from './support-chat/support-chat.component';
import { SupportChatCardComponent } from './support-chat/support-chat-card/support-chat-card.component';


@NgModule({
  declarations: [
    SupportComponent,
    FAQComponent,
    CreateRequestComponent,
    OldRequestComponent,
    SupportChatComponent,
    SupportChatCardComponent],

  imports: [
    SharedModule,
    CommonModule,
    SupportRoutingModule
  ]
})
export class SupportModule { }
