import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-create-request',
  templateUrl: './create-request.component.html',
  styleUrls: ['./create-request.component.scss']
})
export class CreateRequestComponent implements OnInit {
  file: File;

  form = new FormGroup({
    title: new FormControl(''),
    text: new FormControl('')
  });
  constructor() { }

  ngOnInit() {
  }
  onLoadDocument(event: any) {

    const file: File = event.target.files[0] || null;
    this.file = file;
    console.log(this.file);
  }
}
