import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SupportComponent } from './support.component';

import { CreateRequestComponent } from './create-request/create-request.component';
import { FAQComponent } from './faq/faq.component';
import { OldRequestComponent } from './old-request/old-request.component';
import { SupportChatComponent } from './support-chat/support-chat.component';

const routes: Routes = [
  {
    path: '',
    component: SupportComponent,
    // data: { animation: 'Support' },
    children: [
      {
        path: 'faq',
        component: FAQComponent,
        // data: { animation: 'Profile' }
      },
      {
        path: 'create_request',
        component: CreateRequestComponent,
        // data: { animation: 'CreateRequest' }
      },
      {
        path: 'old_request',
        component: OldRequestComponent,
        // data: { animation: 'OldRequest' }
      },
      {
        path: 'supprt_chat',
        component: SupportChatComponent,
        // data: { animation: 'SupportChat' }
      },

      { path: '', redirectTo: 'faq', pathMatch: 'full' },

    ]

  },

];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SupportRoutingModule { }
