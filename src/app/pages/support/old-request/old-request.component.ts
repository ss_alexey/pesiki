import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

type Status = 'hold' | 'progress' | 'complete';
type Topic = 'money' | 'tech problem';
export interface Request {
  id: string | number;
  topic: Topic;
  title: string;
  date: number;
  lastUpdate: number;
  status: Status;
}

const item: Request = {
  id: Math.random().toFixed(6),
  topic: 'money',
  title: 'Nam euismod, urna at efficitur molestie, felis dolor varius urna...',
  date: Date.now(),
  lastUpdate: Date.now(),
  status: 'complete'
};
const list = new Array(10).fill(item);

@Component({
  selector: 'app-old-request',
  templateUrl: './old-request.component.html',
  styleUrls: ['./old-request.component.scss']
})
export class OldRequestComponent implements OnInit {
  requestList: Request[] = (new Array(10).fill(item)).map((i: Request, index) => {
    if (index === 0) {
      // tslint:disable-next-line:max-line-length
      return {
        ...i,
        // tslint:disable-next-line:max-line-length
        title: 'Nam euismod, urna at efficitur molestie, felis dolor varius urna. Nam euismod, urna at efficitur molestie, felis dolor varius urna.',
        topic: 'tech problem',
        status: 'hold'
      } as Request;
    }
    return i;
  });
  // search = new FormControl('');
  form = new FormGroup({
    search: new FormControl('')
  });
  constructor() { }

  ngOnInit() {
  }

}
