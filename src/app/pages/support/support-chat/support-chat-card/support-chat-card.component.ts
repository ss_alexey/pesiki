import { Component, OnInit, Input, AfterContentInit } from '@angular/core';

import { ChatItem } from '../support-chat.component';
// import { from } from 'rxjs';

@Component({
  selector: 'app-support-chat-card',
  templateUrl: './support-chat-card.component.html',
  styleUrls: ['./support-chat-card.component.scss']
})
export class SupportChatCardComponent implements OnInit, AfterContentInit {
  @Input()
  chatItem: ChatItem;
  constructor() { }

  ngOnInit() {
    console.log(this.chatItem);

  }
  ngAfterContentInit() {
    // window.scrollI
    document.body.scrollIntoView(false);
  }
}
