import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupportChatCardComponent } from './support-chat-card.component';

describe('SupportChatCardComponent', () => {
  let component: SupportChatCardComponent;
  let fixture: ComponentFixture<SupportChatCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupportChatCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupportChatCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
