import { Component, OnInit, Host, ElementRef } from '@angular/core';

import { Request } from '../old-request/old-request.component';

export interface ChatItem {

  owner: {
    name: string,
    avatar: string
  };
  text: string;
  date: number;
  file?: File;
}
const chatItem: ChatItem = {
  owner: {
    name: 'Pharrell Williams',
    // tslint:disable-next-line:max-line-length
    avatar: 'https://klike.net/uploads/posts/2019-03/1551511801_1.jpg'
  },
  // tslint:disable-next-line:max-line-length
  text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo, minus adipisci? Placeat, reiciendis assumenda numquam sunt suscipit porro rerum? Laudantium reprehenderit, molestias impedit atque quis quam! Animi modi sequi autem?',
  date: Date.now(),
  file: new File(['asd'], 'Some name of file.png')
};
@Component({
  selector: 'app-support-chat',
  templateUrl: './support-chat.component.html',
  styleUrls: ['./support-chat.component.scss']
})
export class SupportChatComponent implements OnInit {
  file: File;
  chatList: any[] = new Array(2).fill(chatItem).map((item: ChatItem, index: number) => {
    let i: ChatItem;
    i = item;
    if (index === 1) {
      i = {
        ...item, owner: {
          name: ' Radja Koduri',
          // tslint:disable-next-line:max-line-length
          avatar: 'https://store.playstation.com/store/api/chihiro/00_09_000/container/RU/ru/99/EP2402-CUSA05624_00-AV00000000000098//image?_version=00_09_000&platform=chihiro&w=720&h=720&bg_color=000000&opacity=100'
        }
      };
    }
    return i;
  });
  request: Request = {
    id: 123,
    topic: 'money',
    title: 'asdasdasd asdasd asd d asd wera wer ad erw rasdf ',
    date: Date.now(),
    lastUpdate: Date.now(),
    status: 'progress'
  };
  constructor() { }

  ngOnInit() {

  }
  onLoadDocument(event: any) {

    const file: File = event.target.files[0] || null;
    this.file = file;
  }
}
