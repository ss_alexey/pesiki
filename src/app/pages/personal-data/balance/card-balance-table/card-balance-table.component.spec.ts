import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardBalanceTableComponent } from './card-balance-table.component';

describe('CardBalanceTableComponent', () => {
  let component: CardBalanceTableComponent;
  let fixture: ComponentFixture<CardBalanceTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardBalanceTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardBalanceTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
