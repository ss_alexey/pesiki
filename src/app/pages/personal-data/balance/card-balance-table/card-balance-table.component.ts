import { Component, OnInit, ViewChild } from '@angular/core';
// import { MatSort, MatTableDataSource } from '@angular/material';
import { dropDownAnimation } from '@animations';
import { CurrencyService, DeviceService, Device } from '@services';
import { usedCurrencies, currencyNames } from '@config';
import { Router } from '@angular/router';


interface ListItem {
  symbol: string;
  name: string;
  total: string;
  order: string;
  btc_value: string;
  disabled: boolean;
  deposit?: boolean;
}

@Component({
  selector: 'app-card-balance-table',
  templateUrl: './card-balance-table.component.html',
  styleUrls: ['./card-balance-table.component.scss'],
  animations: [dropDownAnimation]
})
export class CardBalanceTableComponent implements OnInit {
  showExpanded: boolean;
  device: Device;
  showDeposit: boolean;
  showSymbol: string;
  show: any;
  list: ListItem[];
  constructor(private currencyService: CurrencyService, private deviceService: DeviceService, private router: Router) {

  }
  toggleshowExpanded = (item: string) => {
    this.show = item;
    if (this.showExpanded) {
      this.showExpanded = false;
    }
    this.showExpanded = true;
  }
  showDeposiWithraw(deposit: boolean, listItem: ListItem) {
    if (this.device === 'mobile') {
      const endpoint = deposit ? 'deposit' : 'withdraw';
      console.log('HERE');
      this.router.navigateByUrl(`profile/balance/${endpoint}`);

    }
    this.show = listItem;
    this.showDeposit = deposit;
  }
  ngOnInit() {
    this.deviceService.device.subscribe(res => {
      this.device = res;
      let fixed = 8;
      if (res === 'tablet') {
        fixed = 3;
      }
      this.list = usedCurrencies.map((curr, index) => {
        return {
          symbol: curr,
          name: currencyNames[curr],
          total: Math.random().toFixed(fixed),
          order: Math.random().toFixed(fixed),
          btc_value: Math.random().toFixed(fixed),
          disabled: index ? false : true
        };
      });
    });
  }

}
