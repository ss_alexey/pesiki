import { Component, OnInit, Input, ElementRef } from '@angular/core';
import { currencyWallet } from '@config';
import { QrCodeService, Device, DeviceService } from '@services';
import { dropDownAnimation } from '@animations';
@Component({
  selector: 'app-deposit-block',
  templateUrl: './deposit-block.component.html',
  styleUrls: ['./deposit-block.component.scss'],
  animations: [dropDownAnimation]
})
export class DepositBlockComponent implements OnInit {
  @Input()
  selectedCurrency: string;
  device: Device;
  selectedCurrencyWallet: string;
  qrImage: string;
  showQrCode = false;
  showWalletAdress = false;
  constructor(private qrCodeService: QrCodeService, private deviceService: DeviceService, private el: ElementRef) { }

  ngOnInit() {
    console.log(
      this.el.nativeElement.classList.add('card')
    );


    this.deviceService.device.subscribe(res => {
      if (res === 'mobile') {
        this.el.nativeElement.classList.add('card');
      } else {
        this.el.nativeElement.classList.remove('card');
      }
    })
    this.selectedCurrencyWallet = currencyWallet[this.selectedCurrency];
    this.qrImage = this.qrCodeService.generateQrImage(this.selectedCurrency);
  }
  enableshowQrCode = () => {
    this.showQrCode = true;
  }
  enableshowWalletAdress = () => {
    this.showWalletAdress = true;
  }
}
