import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DepositBlockComponent } from './deposit-block.component';

describe('DepositBlockComponent', () => {
  let component: DepositBlockComponent;
  let fixture: ComponentFixture<DepositBlockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DepositBlockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DepositBlockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
