
import { Component, OnInit } from '@angular/core';



type Status = 'varified' | 'inProgress' | 'rejected' | 'empty';
interface Document {
  documentTitle: string;
  description: string;
  lastUpdate: number | null;
  fileName: string;
  comment: string;
  status?: string;
}

const files = ['image.jpg', 'image.jpg', 'imagege.jpg', null];
const types = ['ERC20 adress', 'Passport Id', 'Photo with to confirm verification', 'Photo with date to confirm verification'];
const comment = ' Nullam velit est, lobortis finibus dui quis, semper maximus ante.';
const statuses = ['varified', 'inProgress', 'rejected', 'empty'];
// tslint:disable-next-line:max-line-length
// const status
// tslint:disable-next-line:max-line-length
const description = 'Suspendisse eget enim et enim rutrum pulvinar. Nullam velit est, lobortis finibus dui quis, semper maximus ante. Pellentesque eget sem dui. ';
const fileList = (): Document[] => {
  return files.map((file, index) => {
    const newFile = {
      documentTitle: types[index],
      description,
      lastUpdate: types[index] && Date.now() || null,
      fileName: file,
      comment,
      status: statuses[index]
    };
    return newFile;
  }).splice(0, 2);
};
@Component({
  selector: 'app-documents',
  templateUrl: './documents.component.html',
  styleUrls: ['./documents.component.scss']
})
export class DocumentsComponent implements OnInit {

  userDocuments: Document[] = fileList();

  constructor(

  ) { }

  ngOnInit() {

  }
}

