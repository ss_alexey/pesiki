import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-document-card',
  templateUrl: './document-card.component.html',
  styleUrls: ['./document-card.component.scss']
})
export class DocumentCardComponent implements OnInit {
  @Input()
  file: {
    documentTitle: string;
    description: string;
    lastUpdate: number;
    fileName: string;
    comment: string;
    status: string
  };

  constructor() { }
  public onLoadDocument(event: any, documentType: any) {
    const file: File = event.target.files[0] || null;
    this.file.fileName = file.name;
    this.file.status = 'inProgress';
  }
  ngOnInit() {
    // console.log(this.file);
  }

}
