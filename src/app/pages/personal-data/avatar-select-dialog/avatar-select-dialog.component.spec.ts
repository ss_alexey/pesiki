import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AvatarSelectDialogComponent } from './avatar-select-dialog.component';

describe('AvatarSelectDialogComponent', () => {
  let component: AvatarSelectDialogComponent;
  let fixture: ComponentFixture<AvatarSelectDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AvatarSelectDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AvatarSelectDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
