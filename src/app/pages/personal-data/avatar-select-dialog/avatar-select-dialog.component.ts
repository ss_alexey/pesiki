import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-avatar-select-dialog',
  templateUrl: './avatar-select-dialog.component.html',
  styleUrls: ['./avatar-select-dialog.component.scss']
})
export class AvatarSelectDialogComponent implements OnInit {
  avatarList: any[] = new Array(48).fill(null).map((item, index) => {
    return { url: `assets/img/avatars/avatar-${index + 1}.svg`, id: index + 1 };
  });
  constructor(public dialogRef: MatDialogRef<AvatarSelectDialogComponent>) { }

  ngOnInit() {
  }
  onNoClick(avatar: { id: number, url: string }): void {
    this.dialogRef.close(avatar);
  }
}
