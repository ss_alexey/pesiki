import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';
import { MatIconModule } from '@angular/material/icon';

import { PersonalDataRoutingModule } from './personal-data-routing.module';
import { PersonalDataComponent } from './personal-data.component';
import { ProfileComponent } from './profile/profile.component';
import { TwoFactorComponent } from './two-factor/two-factor.component';
import { DocumentsComponent } from './documents/documents.component';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatTabsModule } from '@angular/material/tabs';

import {MatDatepickerModule} from '@angular/material/datepicker';

import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';

import { TfCardComponent } from './two-factor/tf-card/tf-card.component';
import { TfModalComponent } from './two-factor/tf-modal/tf-modal.component';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';


import { MatDialogModule } from '@angular/material/dialog';
import { OldRequestComponent } from './old-request/old-request.component';
import { DocumentCardComponent } from './documents/document-card/document-card.component';

import { ProfileNavigationComponent } from './profile-navigation/profile-navigation.component';
import { BalanceComponent } from './balance/balance.component';
import { CardBalanceTableComponent } from './balance/card-balance-table/card-balance-table.component';
import { DepositBlockComponent } from './balance/card-balance-table/deposit-block/deposit-block.component';
import { WithdrawBlockComponent } from './balance/card-balance-table/withdraw-block/withdraw-block.component';
import { CreateRequestComponent } from './create-request/create-request.component';
import { AvatarSelectDialogComponent } from './avatar-select-dialog/avatar-select-dialog.component';
import { MatNativeDateModule } from '@angular/material';
// import { TransactionsComponent } from '../transactions/transactions.component';



@NgModule({
  declarations: [PersonalDataComponent,
    ProfileNavigationComponent,
    ProfileComponent,
    TwoFactorComponent,
    DocumentsComponent,
    TfCardComponent,
    TfModalComponent,
    OldRequestComponent,
    DocumentCardComponent,
    BalanceComponent,
    CardBalanceTableComponent,
    DepositBlockComponent,
    WithdrawBlockComponent,
    CreateRequestComponent,
    AvatarSelectDialogComponent,
    // TransactionsComponent
  ],

  imports: [
    // MatNativeDateModule,
    // MatDatepickerModule,
    MatSortModule,
    MatSidenavModule,
    MatTableModule,
    // BrowserAnimationsModule,
    MatTooltipModule,
    MatDialogModule,
    MatInputModule,
    // MatSelectModule,
    MatFormFieldModule,
    MatIconModule,
    SharedModule,
    MatTabsModule,
    CommonModule,
    PersonalDataRoutingModule
  ],
  entryComponents: [TfModalComponent, AvatarSelectDialogComponent],
  // providers: [MatNativeDateModule]
})
export class PersonalDataModule { }
