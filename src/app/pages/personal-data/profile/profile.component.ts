import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { DeviceService, UserService } from '@services';
import { UserParams } from '@interfaces';
import { Subscriber, Subscription } from 'rxjs';
import { MatDialog } from '@angular/material';

import { AvatarSelectDialogComponent } from '../avatar-select-dialog/avatar-select-dialog.component';

import * as iso from 'i18n-iso-countries';

declare var require: any;

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit, OnDestroy {
  subscribers: Subscription[] = [];
  user: UserParams;
  device: string;
  states: any;

  form = new FormGroup({
    email: new FormControl('', [Validators.email, Validators.required]),
    avatar: new FormControl(1),
    firstName: new FormControl(''),
    lastName: new FormControl(''),
    username: new FormControl('', [Validators.required, Validators.minLength(2)]),
    phoneNumber: new FormControl(''),
    birthday: new FormControl(''),
    male: new FormControl(null),
    state: new FormControl(null)
  });
  selectedMessenger: any = {};
  showInput = false;

  @Input() userMessengers: any[] = [{ label: 'Wichat', name: 'wichat', img: 'assets/img/messengers/wichat.svg', value: '' },
  { label: 'WhatsApp', name: 'whatsapp', img: 'assets/img/messengers/whatsapp.svg', value: '' },
  { label: 'Telegram', name: 'telegram', img: 'assets/img/messengers/telegram.svg', value: '' },
  { label: 'Viber', name: 'viber', img: 'assets/img/messengers/viber.svg', value: '' }

  ];
  // @Output() onUpdatedMessengers: EventEmitter<any> = new EventEmitter<any>();

  @ViewChild('input') inputMessage: ElementRef;

  constructor(
    public dialog: MatDialog,
    private deviceService: DeviceService,
    private userService: UserService
  ) {
    deviceService.device.subscribe(res => {
      this.device = res;
    });
  }

  showMessengerValue(messenger: any) {
    this.showInput = true;
    this.selectedMessenger = this.userMessengers.find(
      mess => mess.name === messenger.name
    );

    setTimeout(() => {
      this.inputMessage.nativeElement.focus();
    }, 100);
  }

  updateMessengers(value) {
    if (this.selectedMessenger.value !== value) {
      if (value.length && value.length < 4) {
        this.showInput = false;
        return;
      }
      this.selectedMessenger.value = value;
    }
    this.showInput = false;
  }
  updateUserParams() {
    this.userService.updateUserProfile(this.form.value);
  }
  openAvatarSelectDialog() {
    const dialogRef = this.dialog.open(AvatarSelectDialogComponent, {
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.user = { ...this.user, AvatarUrl: result.url };
        this.form.controls.avatar.setValue(result.id);
      }
    });
  }
  ngOnInit() {
    iso.registerLocale(require('i18n-iso-countries/langs/en.json'));
    this.states = Object.values(iso.getNames('en'));
    this.subscribers.push(this.userService.user.subscribe(res => {
      console.log(res);
      this.user = res;
    }));

    const { Phone, Email, Username } = this.user;
    this.form.patchValue({ firstName: Username, phone: Phone, email: Email });
    this.form.valueChanges.subscribe(res => {
      console.log(res);

    });
  }
  ngOnDestroy() {
    if (this.subscribers && this.subscribers.length) {
      this.subscribers.forEach(i => {
        i.unsubscribe();
      });
    }

  }
}
