import { Component, OnInit, OnDestroy } from '@angular/core';
import { RouterOutlet } from '@angular/router';

import { Store, Section } from '../../store';
import { slideInAnimation } from '../../a';
import { sideNavAnimation } from '../../../animations/sideNavAnimation';

import { DeviceService } from '@app/services';

@Component({
  selector: 'app-personal-data',
  templateUrl: './personal-data.component.html',
  styleUrls: ['./personal-data.component.scss'],
  animations: [
    slideInAnimation,
    sideNavAnimation
    // animation triggers go here
  ]
})
export class PersonalDataComponent implements OnInit, OnDestroy {
  // device: string;
  // sideNavOpen: any;
  device: 'mobile' | 'tablet' | 'fullscreen' | any;
  constructor(private store: Store, private deviceService: DeviceService) {
    deviceService.device.subscribe(res => {
      this.device = res;
    });
    // this.store.select(Section.sideNavOpen).subscribe((open) => {
    //   this.sideNavOpen = open;
    // });
    // this.store.select(Section.device).subscribe((device) => {
    //   this.device = device;
    // });
  }



  prepareRoute(outlet: RouterOutlet) {
    // console.log(outlet);
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation'];
  }
  ngOnInit() {
    console.log('activated');

  }
  ngOnDestroy() {
    console.log('disactivated');
  }

}
