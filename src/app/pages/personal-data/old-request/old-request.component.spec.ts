import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OldRequestComponent } from './old-request.component';

describe('OldRequestComponent', () => {
  let component: OldRequestComponent;
  let fixture: ComponentFixture<OldRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OldRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OldRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
