import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TfCardComponent } from './tf-card.component';

describe('TfCardComponent', () => {
  let component: TfCardComponent;
  let fixture: ComponentFixture<TfCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TfCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TfCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
