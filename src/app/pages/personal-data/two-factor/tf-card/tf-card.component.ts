// import { Component, OnInit, AfterViewInit } from '@angular/core';
import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  AfterViewInit
} from '@angular/core';
import {
  TwoFactorAction,
  TwoFactorSettings,
  TwoFactorMethods,
  Enable2Fa
} from '../../../../../_services/twofactor.service';

import { TwoFactorService } from '../../../../../_services';

import { FormGroup, FormBuilder } from '@angular/forms';

import { TfModalComponent } from '../tf-modal/tf-modal.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-tf-card',
  templateUrl: './tf-card.component.html',
  styleUrls: ['./tf-card.component.scss']
})
export class TfCardComponent implements OnInit, AfterViewInit {

  tfForm: FormGroup;

  @Input() methods: TwoFactorMethods[] = null;
  @Input() settings: TwoFactorSettings | TwoFactorSettings[];
  @Input() action: TwoFactorAction = null;
  ngAfterViewInit() {
    // console.log(this.methods, this.settings, this.action);
  }
  ngOnInit() {
    console.log(this.methods);
    console.log(this.settings);
    console.log(this.action);
  }
  // @Output() update2fa = new EventEmitter();

  // private newFromValue = 0;
  // animal: string;
  // name: string;

  constructor(public dialog: MatDialog, private tfService: TwoFactorService) { }
  update2fa(event) {
    console.log(11111111, event);
    // action: "Auth"
    // options: { }
    // type: "Google2FA"
    // const options = this.settings.options;
    // const payload: Enable2Fa = { type: event,  options : this.settings.options, action: this.action.name };

    // this.tfService.on2fa(payload).toPromise().then(res => {
    //   // console.log(1111111, res);
    //   this.openDialog(res);
    // }).catch(e => {
    //   console.error('ERROR', e);
    // });
    // if (event.status === 'on') {
    //   const payload: Enable2Fa = {
    //     action: event.action,
    //     options: event.options,
    //     type: event.type
    //   };
    //   this.tfService
    //     .on2fa(payload)
    //     .pipe(map(item => this.entitiesUserOptions(item)))
    //     .subscribe(response => {
    //       this.showModal = { ...event, ...response };
    //     });
    //   // console.log(11111, this.showModal);

    // } else {
    //   this.tfService.off2fa(event.id).subscribe(
    //     response => {
    //       this.showModal = { ...event };
    //     },
    //     err => this.msgSerice.show2FaMsg(err.error.Message)
    //   );
    // }
  }
  openDialog(params): void {
    console.log('OPEn');

    const dialogRef = this.dialog.open(TfModalComponent, {
      // width: '250px',
      // data: { name: 'this.name', animal: 'this.animal' }
      data: { params, action: this.action }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      // this.animal = result;
    });
  }
  // // constructor() { }

  get isActionFrom() {
    if (!this.action) {
      return false;
    }
    return !!this.action.options.find(option => option.key === 'from');
  }

  // get setttingActionFrom() {
  //   if (!this.settings) {
  //     return 0;
  //   }

  //   const from = this.settings.options.hasOwnProperty('from')
  //     ? this.settings.options['from']
  //     : 0;
  //   return from;
  // }

  // set setttingActionFrom(value) {
  //   this.newFromValue = value;
  // }

  // ngOnInit() { }

  // ngAfterViewInit(): void { }

  // enable2fa(type) {
  //   console.log('ENABLE', type);

  //   const params = {};
  //   params['type'] = type;
  //   params['action'] = this.action.name;
  //   params['options'] = {};

  //   if (this.action && !!this.action.options.length) {
  //     params['options'] = {
  //       from: this.newFromValue || this.setttingActionFrom
  //     };
  //   }
  //   const action = { status: 'on', ...params };
  //   // console.log('EMIT:::', action);
  //   this.update2fa.emit(action);
  // }

  // disable2fa() {
  //   const params = {};
  //   params['action'] = this.action;
  //   params['id'] = this.settings['id'];
  //   params['type'] = this.settings['type'];

  //   const action = { status: 'off', ...params };
  //   this.update2fa.emit(action);
  // }

}
