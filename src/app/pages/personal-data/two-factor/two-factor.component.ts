import { Component, OnInit, AfterViewInit } from '@angular/core';
import {dropDownAnimation} from '@animations';
// import {
//   TwoFactorService,
//   TwoFactorMethods,
//   TwoFactorAction,
//   TwoFactorSettings,
//   Enable2Fa
// } from '@app/_services/twofactor.service';
import {
  TwoFactorService,
  TwoFactorMethods,
  TwoFactorAction,
  TwoFactorSettings,
  Enable2Fa
} from '../../../../_services/twofactor.service';
import { map } from 'rxjs/operators';
// import { MsgService } from '@services';
// import { MsgService } from '../../../../_services';

@Component({
  selector: 'app-two-factor',
  templateUrl: './two-factor.component.html',
  styleUrls: ['./two-factor.component.scss'],
  animations: [
    dropDownAnimation
  ]
})
export class TwoFactorComponent implements OnInit {
  showDescription = false;

  methods: TwoFactorMethods[] = null;
  actions: TwoFactorAction[] = null;
  settings: { [key: string]: TwoFactorSettings } = null;

  showModal: {} = null;

  constructor(
    private tfService: TwoFactorService,
    // private msgSerice: MsgService
  ) { }
  toggleDescription() {
    this.showDescription = !this.showDescription;
  }
  ngOnInit() {
    // console.log(this.methods);
    // console.log(this.actions);
    // console.log(this.settings);

    this.getTwoFactorMethods();
    this.getTwoFactorActions();
    this.getUserSettings();
  }

  private getTwoFactorMethods() {
    this.tfService.getAllTwoFactor().subscribe(
      twoFactors => {
        console.log('TWO FACTOR METHODS:::', twoFactors);
        this.methods = twoFactors;
      },
      err => {
        console.log('TWO FACTOR METHODS ERROR:::', err);
      }
    );
  }

  private getTwoFactorActions() {
    this.tfService.getAllActions().subscribe(
      twoFactorsActions => {
        console.log('TWO FACTOR ACTIONS:::', twoFactorsActions);
        this.actions = twoFactorsActions;
      },
      err => {
        console.log('TWO FACTOR ACTIONS ERROR:::', err);
      }
    );
  }

  private entitiesUserOptions(item) {
    const from =
      (item.options as Array<any>).find(opt => opt.key === 'from') || null;

    item.options = from ? { from: from.value } : {};

    return { ...item };
  }

  private getUserSettings() {
    this.tfService
      .getUserSettings()
      .pipe(
        map(settings => {
          const settingsObj = settings.reduce((prev, next) => {
            return { ...prev, [next.action]: this.entitiesUserOptions(next) };
          }, {});

          return settingsObj;
        })
      )
      .subscribe(
        settings => {
          console.log('Settings:::', settings);
          this.settings = { ...settings };
        }
        // err => this.msgSerice.show2FaMsg(err.error)
      );
  }

  getActionUserSettings(actionName: string) {
    if (!this.settings) {
      return;
    }
    return this.settings[actionName] ? this.settings[actionName] : null;
  }

  update2fa(event) {
    if (event.status === 'on') {
      const payload: Enable2Fa = {
        action: event.action,
        options: event.options,
        type: event.type
      };
      this.tfService
        .on2fa(payload)
        .pipe(map(item => this.entitiesUserOptions(item)))
        .subscribe(response => {
          this.showModal = { ...event, ...response };
        });
    } else {
      this.tfService.off2fa(event.id).subscribe(
        response => {
          this.showModal = { ...event };
        },
        // err => this.msgSerice.show2FaMsg(err.error.Message)
      );
    }
  }

  accept2fa(code) {
    this.tfService.acceptAction(code.id, code.code).subscribe(
      response => {
        this.closeModal();
        this.getUserSettings();
      },
      err => {
        // TODO: need set up different messages in backend
        // this.msgSerice.show2FaMsg('incorrectCode');
      }
    );
  }

  closeModal() {
    this.showModal = null;
  }

  resendCode() {
    // Call again update method for resending code;
    this.update2fa(this.showModal);
  }

  // selectedMessenger: any = {};


  // showInput = false;

  // @Input() userMessengers: any[];
  // // @Output() onUpdatedMessengers: EventEmitter<any> = new EventEmitter<any>();

  // @ViewChild('input') inputMessage: ElementRef;

  // // constructor(private msgService: MsgService) {}
  // constructor() { }
  // showMessengerValue(messenger: any) {
  //   console.log(this.selectedMessenger);
  //   this.showInput = true;
  //   this.selectedMessenger = this.userMessengers.find(
  //     mess => mess.name === messenger.name
  //   );

  //   setTimeout(() => {
  //     // <HTMLInputElement>this.inputMessage.nativeElement.focus();
  //   }, 100);
  // }

  // updateMessengers(value) {
  //   if (this.selectedMessenger.value !== value) {
  //     if (value.length && value.length < 4) {
  //       // this.msgService.showProfileError('MessengerNameMinLength');
  //       this.showInput = false;
  //       return;
  //     }
  //     this.selectedMessenger.value = value;

  //     // this.onUpdatedMessengers.emit(this.userMessengers);
  //   }
  //   this.showInput = false;
  // }

}
