import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnDestroy,
  Inject
} from '@angular/core';
import { interval } from 'rxjs';
import { take, map } from 'rxjs/operators';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-tf-modal',
  templateUrl: './tf-modal.component.html',
  styleUrls: ['./tf-modal.component.scss']
})
export class TfModalComponent implements OnInit, OnDestroy {



  subscribtions = [];
  constructor(
    public dialogRef: MatDialogRef<TfModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    console.log('DATA', data);

  }
  // action: "Auth"
  // id: 391
  // options: []
  // secret: "ibQLTtcH3v"
  // type: "Google2FA"
  // url: "http://chart.gooRRUQM3W%26issuer%3DSigma%2520Auth"

//   action: "Auth"
// id: 398
// options: []
// secret: "812480"
// type: "Sms"
// url: null
  ngOnDestroy() { }
  ngOnInit() {
    if (this.data.params.type === 'Google2FA') {
      this.data.description = 'Scan this QR code in the Google Authentication app';
      this.data.title = 'Two-Factor Authentication';
    }
    if (this.data.params.type === 'Sms') {
      this.data.description = 'Pin-code sent to your phone please enter code below';
      this.data.title = 'Pin-code to sms';
    }
    console.log(this.data);

  }
  closeModal(): void {
    this.dialogRef.close();
  }

  // openAcceptGoogle2fa() {
  //   this.modalName = 'GoogleAccept';
  // }

  // confirm2fa(code) {
  //   console.log('PARAMS:::', this.parameters);
  //   this.aceptCode.emit({ code, id: this.parameters.id });
  // }

  // cancel() {
  //   this.close.emit();
  // }

  // resend() {
  //   this.resendCode.emit();
  //   this.startTimer();
  // }

  // ngOnInit() {
  //   this.modalName = this.parameters.type;
  // }

  // private startTimer(timer = 30) {
  //   this.timer = timer;
  //   this.subscribtions.push(
  //     interval(1000)
  //       .pipe(
  //         take(timer + 1),
  //         map(secondsFormStart => timer - secondsFormStart)
  //       )
  //       .subscribe(remainingTime => {
  //         this.timer = remainingTime;
  //         console.log(this.timer);
  //       })
  //   );
  // }

  // ngOnDestroy(): void {
  //   this.subscribtions.forEach(subscribtion => {
  //     subscribtion.unsubscribe();
  //   });
  // }

}
