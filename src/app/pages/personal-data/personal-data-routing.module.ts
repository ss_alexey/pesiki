import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PersonalDataComponent } from './personal-data.component';
import { ProfileComponent } from './profile/profile.component';
import { TwoFactorComponent } from './two-factor/two-factor.component';
import { DocumentsComponent } from './documents/documents.component';
import { OldRequestComponent } from './old-request/old-request.component';
import { BalanceComponent } from './balance/balance.component';
import { DepositBlockComponent } from './balance/card-balance-table/deposit-block/deposit-block.component';
import { WithdrawBlockComponent } from './balance/card-balance-table/withdraw-block/withdraw-block.component';
import { CreateRequestComponent } from './create-request/create-request.component';
// import { TransactionsComponent } from '../transactions/transactions.component';

const routes: Routes = [
  {
    path: '',
    component: PersonalDataComponent,
    data: { animation: 'test' },
    children: [
      // {
      //   path: 'transactions',
      //   component: TransactionsComponent,
      //   data: { animation: 'Transaction' }
      // },
      {
        path: 'profile',
        component: ProfileComponent,
        data: { animation: 'Profile' }
      },
      {
        path: 'tf',
        component: TwoFactorComponent,
        data: { animation: 'TF' }
      },
      {
        path: 'document',
        component: DocumentsComponent,
        data: { animation: 'Document' }
      },
      {
        path: 'old_request',
        component: OldRequestComponent,
        data: { animation: 'OldRequest' }
      },
      {
        path: 'balance',
        component: BalanceComponent,
        data: { animation: 'Balance' }
      },
      {
        path: 'balance/deposit',
        component: DepositBlockComponent,
        data: { animation: 'Deposit' }
      },
      {
        path: 'balance/withdraw',
        component: WithdrawBlockComponent,
        data: { animation: 'Withdraw' }
      },
      {
        path: 'request/createRequest',
        component: CreateRequestComponent,
        data: { animation: 'CreateRequest' }
      },
      { path: '', redirectTo: 'profile', pathMatch: 'full' },

    ]

  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PersonalDataRoutingModule { }
