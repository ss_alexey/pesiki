import { Component, OnInit, OnDestroy } from '@angular/core';

import { TransactionService } from '@services';

import { QrCodeService } from '../../services/qr-code.service';
import { Router } from '@angular/router';

import { TransactionPostRequest, TransactionParams } from '@interfaces';
import { currencyWallet } from '@config';
import { FormValidatorService } from '@services';


import { FormControl, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-payment-detail',
  templateUrl: './payment-detail.component.html',
  styleUrls: ['./payment-detail.component.scss']
})


export class PaymentDetailComponent implements OnInit, OnDestroy {
  // subscribtions: Subscription[] = [];
  configWallet: string;
  form = new FormGroup({
    // wallet: new FormControl('', this.formValidator.validateWallet('BTC'))
  });
  dogHelperOpen = false;
  payment: TransactionParams;
  qrImg: string;
  copied: boolean;
  constructor(
    private formValidator: FormValidatorService,
    private router: Router,
    private qrImgService: QrCodeService,
    private transactionService: TransactionService,
  ) {
    qrImgService.paymentDetails.subscribe(res => {
      this.payment = res;
    });
    if (!this.payment) {
      router.navigateByUrl('deposit');
    }
    this.form.addControl('wallet', new FormControl('', this.formValidator.validateWallet(this.payment.currency)));
    this.configWallet = currencyWallet[this.payment.currency];
  }
  sendTransaction =  (e: Event) => {
    console.log(this.form.controls.wallet.value);

    const payment: TransactionPostRequest = {
      addressFrom: this.form.controls.wallet.value,
      addressTo: this.configWallet,
      amount: this.payment.amount,
      currency: this.payment.currency,
      cdsToken: this.payment.cdsToken
    };
    e.preventDefault();
    console.log(payment);

    console.log(this.configWallet);
    console.log(this.form.valid);
    
    
    if (this.configWallet ) {
      this.transactionService.sendTransaction(payment);
      // this.router.navigateByUrl('profile/transactions');
    }

  }
  close() {
    this.dogHelperOpen = false;
  }
  copy = () => {
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = this.configWallet;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    this.copied = true;
    // console.log(this.copied);

  }
  ngOnInit() {
    // console.log(this.payment);
    setTimeout(() => {
      this.dogHelperOpen = true;
    }, 1000);
    // this.form.controls.wallet.setValue(this.configWallet);
    if (!this.payment) {
      this.router.navigateByUrl('deposit');
    } else {
      this.qrImg = this.qrImgService.generateQrImage(this.payment.currency, this.payment.amount);
    }
  }
  ngOnDestroy() {
    // this.subscribtions.forEach(i => {
    //   i.unsubscribe();
    // });
  }
}
