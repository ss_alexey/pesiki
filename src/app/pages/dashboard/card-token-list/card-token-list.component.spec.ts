import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardTokenListComponent } from './card-token-list.component';

describe('CardTokenListComponent', () => {
  let component: CardTokenListComponent;
  let fixture: ComponentFixture<CardTokenListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardTokenListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardTokenListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
