import { Component, OnInit, OnDestroy } from '@angular/core';

import { UserService } from '@services';
import { Subscriber, Subscription } from 'rxjs';
import { UserParams } from '@/interfaces';

@Component({
  selector: 'app-card-profile',
  templateUrl: './card-profile.component.html',
  styleUrls: ['./card-profile.component.scss']
})
export class CardProfileComponent implements OnInit, OnDestroy {
  user: UserParams;
  subsribtions: Subscription[] = [];
  constructor(private userService: UserService) { }

  ngOnInit() {
    this.subsribtions.push(this.userService.user.subscribe(res => {
      console.log(res);

      this.user = res;
    }));
  }
  ngOnDestroy() {
    this.subsribtions.forEach(i => {
      i.unsubscribe();
    });
  }
}
