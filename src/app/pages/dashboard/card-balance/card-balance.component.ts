import { Component, OnInit } from '@angular/core';

import { usedCurrencies } from '../../../config';
import { TransactionService } from '@/app/services';
import { TransactionParams } from '@/interfaces';
import { Router } from '@angular/router';

@Component({
  selector: 'app-card-balance',
  templateUrl: './card-balance.component.html',
  styleUrls: ['./card-balance.component.scss']
})
export class CardBalanceComponent implements OnInit {
  totalBalance = 0;
  currencies: any[] = usedCurrencies.map(i => {
    console.log(i);

    return `assets/img/new/crypto_logo/${i}-coin.svg`;
  });
  constructor(private transactionService: TransactionService, private router: Router) {
    this.transactionService.getTransactions().toPromise().then((res: TransactionParams[]) => {
      res.filter(i => {
        return i.status === 2;
      }).forEach(b => {
        this.totalBalance = this.totalBalance + b.cdsToken;
      });

    }).catch(e => {
      console.log(e);

    });
  }

  ngOnInit() {
    console.log(this.currencies);

  }
  navigateERC20() {
    console.log('HERE');
    // this.router.navigate('https://etherscan.io')
    // this.router.navigateByUrl('https://etherscan.io');
  }
}
