import { Component, OnInit } from '@angular/core';

import { coinDogBasePrice } from '@config';

import { ApiService } from '@services';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-card-token',
  templateUrl: './card-token.component.html',
  styleUrls: ['./card-token.component.scss']
})
export class CardTokenComponent implements OnInit {
  coinDog = coinDogBasePrice;
  fileUrl;
  constructor(private apiService: ApiService, private sanitizer: DomSanitizer, private httpService: ApiService) { }

  ngOnInit() {
  }

}
