import { Component, OnInit, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ConfigService, CurrencyService } from '@services';
import { Router } from '@angular/router';

@Component({
  selector: 'app-card-token-calculator',
  templateUrl: './card-token-calculator.component.html',
  styleUrls: ['./card-token-calculator.component.scss']
})
export class CardTokenCalculatorComponent implements OnInit {
  @Input()
  calculateETH?: boolean;
  // @Input()
  // calculateTo: string;
  formInput = new FormControl();
  total: { value: number, currency: string } = { value: 0, currency: this.configService.getCoinDogConfig().currency };
  constructor(private configService: ConfigService, private currencyService: CurrencyService, private router: Router) { }
  navigateToPayment = (e: Event) => {
    if (!this.currencyService.getCoinDogAmount()) {
      e.preventDefault();
    } else {
      this.router.navigateByUrl('deposit');
    }
  }
  ngOnInit() {
    if (this.calculateETH) {
      this.total = { value: 0, currency: 'ETH' };
    } else {
      this.total = { value: 0, currency: this.configService.getCoinDogConfig().currency };
    }
    this.formInput.valueChanges.subscribe(res => {
      if (this.calculateETH) {
        const value = this.currencyService.calculateCurrencyPrice(res, 'USD', 'ETH');
        this.total = { ...this.total, value };
        console.log(this.total);
      } else {
        const value = this.configService.getCoinDogConfig().value * res;
        this.total = { ...this.total, value };
        this.currencyService.updateCoinDogAmount(res);

      }
      // const test = this.currencyService.calculateCurrencyPrice(res, 'USD', 'USD');
      // console.log(test);
    });
  }

}
