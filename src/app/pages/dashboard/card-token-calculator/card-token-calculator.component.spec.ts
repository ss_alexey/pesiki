import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardTokenCalculatorComponent } from './card-token-calculator.component';

describe('CardTokenCalculatorComponent', () => {
  let component: CardTokenCalculatorComponent;
  let fixture: ComponentFixture<CardTokenCalculatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardTokenCalculatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardTokenCalculatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
