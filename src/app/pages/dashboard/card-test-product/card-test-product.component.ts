import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-card-test-product',
  templateUrl: './card-test-product.component.html',
  styleUrls: ['./card-test-product.component.scss']
})
export class CardTestProductComponent implements OnInit {

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
  }
  redirectToSite() {
    console.log('navigate');
    // this.router.navigateByUrl('https://dev.coindogs.co');
  }
}
