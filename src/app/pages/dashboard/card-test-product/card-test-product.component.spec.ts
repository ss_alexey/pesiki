import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardTestProductComponent } from './card-test-product.component';

describe('CardTestProductComponent', () => {
  let component: CardTestProductComponent;
  let fixture: ComponentFixture<CardTestProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardTestProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardTestProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
