import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { AngularModule } from './modules/angular.module';
import { MatIconModule } from '@angular/material/icon';
import { ProfileNavigationComponent } from './components/header/profile-navigation/profile-navigation.component';
import { MatFormFieldModule, MatInputModule, MatNativeDateModule, MatDatepickerModule } from '@angular/material';
import { SideNavComponent } from './components/side-nav/side-nav.component';
import { ExpandMenuDirective } from './directives/expand-menu.directive';
import { DropdownComponent } from './components/dropdown/dropdown.component';
import { DogHelperComponent } from './components/dog-helper/dog-helper.component';

import { InputTextComponent } from '../custom-components/input-text/input-text.component';
import { InputTextLabelComponent } from '../custom-components/input-text-label/input-text-label.component';
import { FloatingNotificationComponent } from './components/floating-notification/floating-notification.component';
import { MatSelectModule } from '@angular/material/select';

import {MatRadioModule} from '@angular/material/radio';

@NgModule({
  declarations: [HeaderComponent,
    InputTextLabelComponent,
    FooterComponent,
    SideNavComponent,
    ExpandMenuDirective,
    DropdownComponent,
    DogHelperComponent,
    InputTextComponent,
    ProfileNavigationComponent,
    FloatingNotificationComponent
  ],
  imports: [
    MatSelectModule,
    // MatNativeDateModule,
    MatRadioModule,
    MatDatepickerModule,
    MatInputModule,
    MatFormFieldModule,
    CommonModule,
    AngularModule,
    MatIconModule,
    MatNativeDateModule
  ],
  exports: [
    MatSelectModule,
    MatRadioModule,
    MatNativeDateModule,
    MatDatepickerModule,
    FloatingNotificationComponent,
    InputTextLabelComponent,
    InputTextComponent,
    DogHelperComponent,
    DropdownComponent,
    MatInputModule,
    MatFormFieldModule,
    MatIconModule,
    SideNavComponent,
    HeaderComponent,
    FooterComponent,
    AngularModule,
    // MatNativeDateModule
  ],
  providers: [MatNativeDateModule]

})
export class SharedModule { }
