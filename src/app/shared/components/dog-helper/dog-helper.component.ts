import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { opacityAnimation } from '@animations';

@Component({
  selector: 'app-dog-helper',
  templateUrl: './dog-helper.component.html',
  styleUrls: ['./dog-helper.component.scss'],
  animations: [
    opacityAnimation
  ]
})
export class DogHelperComponent implements OnInit {
  @Output()
  closeHelper = new EventEmitter<boolean>();
  open = false;
  showTip = false;

  constructor() { }

  ngOnInit() {
    setTimeout(() => {
      this.showTip = true;
    }, 1000);
    setTimeout(() => {
      if (this.showTip) {
        this.closeHelper.emit(true);
      }
    }, 8000);
  }
  toggle(value?: boolean) {
    console.log('HERE');
    if (value) {
      this.open = value;
      this.showTip = false;
    }

  }
  close() {
    console.log('11111');
    this.closeHelper.emit(true);
  }
}
