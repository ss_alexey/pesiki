import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DogHelperComponent } from './dog-helper.component';

describe('DogHelperComponent', () => {
  let component: DogHelperComponent;
  let fixture: ComponentFixture<DogHelperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DogHelperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DogHelperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
