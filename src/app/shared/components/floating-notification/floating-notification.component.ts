import { Component, OnInit } from '@angular/core';

import { FloatingErrorService } from '@services';
import { trigger, transition, style, animate } from '@angular/animations';
// error: 1
// warn: 2
// success: 3
const error = {
  status: 1,
  text: 'some error text'
};

@Component({
  selector: 'app-floating-notification',
  templateUrl: './floating-notification.component.html',
  styleUrls: ['./floating-notification.component.scss'],
  animations: [
    trigger('float', [
      transition(':enter', [
        style({
          opacity: 0,
          top: '100vh'

        }),
        animate('0.5s linear', style({
          top: '0vh',
          opacity: 1,
        }))
      ]),
      transition(':leave', [
        style({
          opacity: 1,
        }),
        animate('0.3s  linear', style({
          opacity: 0,
        })),
        style({
          height: '*'

        }),
        animate('0.3s 0.2s  linear', style({
          height: '0'

        }))
      ])
    ])
  ]
})
export class FloatingNotificationComponent implements OnInit {
  errorList;
  constructor(private floatingNotificatinService: FloatingErrorService) { }

  ngOnInit() {
    this.floatingNotificatinService.notifications.subscribe(res => {
      this.errorList = res;
    });
    // this.floatingNotificatinService.addNotification(1, 'asdasdadsdas111');
    // this.floatingNotificatinService.addNotification(2, 'asdasdadsdas222222');
    // this.floatingNotificatinService.addNotification(3, '33333333333');
    // this.floatingNotificatinService.removeNotification(1);
  }
  deleteCard = (index: number) => {
    this.floatingNotificatinService.removeNotification(index);
  }
}
