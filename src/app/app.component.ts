import { Component, OnInit, HostListener, ElementRef } from '@angular/core';
import { Store, Section } from './store';
import * as jwtDecode from 'jwt-decode';
import { RouterOutlet } from '@angular/router';
import { sideNavAnimation } from '@animations';



import { DeviceService, UserService } from '@services';

import { CurrencyService } from './services/currency.service';

import { ViewOptionService, ViewOptions } from './services/view-option.service';

import { slideInAnimation } from './animation';

import { AuthService } from '@services';



// iso.registerLocale(iso.langs.);
// iso.registerLocale(iso('i18n-iso-countries/langs/fr.json'));
// console.log('US (Alpha-2) => ' + iso.getName('US', 'en'));

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    sideNavAnimation,
    slideInAnimation

  ]
})
export class AppComponent implements OnInit {
  viewOptions: ViewOptions;
  device: string;
  text = new Array(10).fill('Just bought 3 ETH in Irland');
  title = 'CoinDogs';


  constructor(
    private el: ElementRef,
    private viewOptionService: ViewOptionService,
    private deviceService: DeviceService,
    private currency: CurrencyService,
    private store: Store,
    private authService: AuthService,
    private userService: UserService
  ) {
    viewOptionService.options.subscribe(res => {
      this.viewOptions = res;
    });
  }

  prepareRoute(outlet: RouterOutlet) {

    if (!outlet.activatedRouteData.test) {
      return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation'];
    }
    console.log('NO ROUTE');
    return;
  }

  setCookie(cname, cvalue, exdays) {
    const d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    const expires = 'expires=' + d.toUTCString();
    console.log(cname + '=' + cvalue + '; ' + expires);

    document.cookie = cname + '=' + cvalue + '; ' + expires;
    // document.cookie = 'login=test@test.com; path=/; domain=.dev.coindogs.co; Expires=Thu, 03 Jun 2021 08:53:45 GMT;';
  }
  // getCookie(cname) {
  //   const name = cname + '=';
  //   const ca = document.cookie.split(';');
  //   // tslint:disable-next-line:prefer-for-of
  //   for (let i = 0; i < ca.length; i++) {
  //     let c = ca[i];
  //     while (c.charAt(0) === ' ') { c = c.substring(1); }
  //     if (c.indexOf(name) === 0) { return c.substring(name.length, c.length); }
  //   }
  //   return '';
  // }

  ngOnInit() {

    if (localStorage.getItem('token')) {
      this.userService.updateUser();
    }
    this.deviceService.device.subscribe(res => {
      this.device = res;
    });
    let count = 0;
    this.currency.tickerData.subscribe(res => {
      count++;
    });

  }
}
