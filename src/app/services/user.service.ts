import { Injectable } from '@angular/core';
import { ReplaySubject } from 'rxjs';

import { UserParams, UpdateUserParamsRequest } from '@interfaces';


import * as jwtDecode from 'jwt-decode';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  public userParams: UserParams;
  public user = new ReplaySubject<UserParams | null>();
  constructor(private apiService: ApiService) {
  }
  updateUser = () => {
    const token = localStorage.getItem('token');
    const user = jwtDecode(token);
    this.userParams = user;
    console.log(user);
    this.user.next(user);
  }
  updateUserProfile = (userParams: UpdateUserParamsRequest) => {
    this.apiService.post('updateProfile/', userParams).toPromise().then((res: { Token: string }) => {
      if (res && res.Token) {
        localStorage.setItem('token', res.Token);
        this.updateUser();
      }
      console.log(res);
    }).catch(e => {
      console.log(e);

    });
  }
}
