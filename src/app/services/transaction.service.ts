import { Injectable } from '@angular/core';
import { ReplaySubject, Subject } from 'rxjs';


import { TransactionPostRequest, TransactionParams, TransactionGetResponse } from '@interfaces';

import { ApiService } from './api.service';

import { UserService } from './user.service';
import { Router } from '@angular/router';

type Status = 'paid' | 'process' | 'timeout';


@Injectable({
  providedIn: 'root'
})
export class TransactionService {
  // public transactions = new ReplaySubject<TransactionParams>();
  transactions: TransactionParams[] = [];
  count: boolean;
  constructor(private apiService: ApiService, private userService: UserService, private router: Router) {
    this.getTransactions();
    // this.transactions.
  }
  public sendTransaction = async (transaction: TransactionPostRequest) => {

    await this.apiService.post('createUserTransactions/', transaction).toPromise().then((res: TransactionParams) => {
      console.log(res);
      this.getTransactions().toPromise().then((resp: any)  => {
        this.transactions = resp;
      });
      this.router.navigateByUrl('profile/transactions');
      // this.apiService.get('getUserTransactions/').toPromise().then((resa: TransactionGetResponse) => {
      //   console.log(res);
      //   this.transactions = new ReplaySubject<TransactionParams>();
      //   resa.forEach(element => {
      //     this.transactions.next(element);
      //   });
      //   // this.transactions.next(res);
      // }).catch(e => {
      //   console.log(e);

      // });
    }).catch(e => {
      console.log(e);
    });
  }

  getTransactions = () => {
    return this.apiService.get('getUserTransactions/');
    // return this.apiService.get('getUserTransactions/').toPromise().then((res: TransactionGetResponse) => {
    //   this.transactions = res;
    //   // if (this.count) {
    //   //   this.transactions.complete();
    //   //   this.transactions = new ReplaySubject<TransactionParams>();
    //   // }
    //   // console.log(res);

    //   // this.count = true;
    //   // res.forEach(element => {
    //   //   this.transactions.next(element);
    //   // });
    //   // this.transactions.next(res);
    // }).catch(e => {
    //   console.log(e);

    // });

  }
}
