import { Injectable } from '@angular/core';

import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { FloatingErrorService } from '@services';

@Injectable({
  providedIn: 'root'
})
export class InterceptorService implements HttpInterceptor {
  intercept(request: HttpRequest<any>, next: HttpHandler
  ): Observable<HttpEvent<any>> {
    // const headers = new HttpHeaders({
    //   'Set-Cookie': 'true',
    //   'Access-Control-Allow-Credentials': 'true',
    //   'Access-Control-Allow-Origin': '*',
    //   'Access-Control-Allow-Headers': '*'
    //   // 'Content-Type': 'application/json',
    //   // withCredentials: 'true',
    //   // 'Access-Control-Allow-Origin': 'true',
    //   // 'Access-Control-Allow-Credentials': 'true'
    // });
    // request = request.clone({
    //   headers,
    //   // withCredentials: true,
    // });
    return next.handle(request).pipe(
      catchError((err: HttpErrorResponse) => {
        this.floadtService.addNotification(1, err.statusText);
        console.log(err);
        return throwError(err);
      })
    );
  }

  constructor(private floadtService: FloatingErrorService) { }
}
