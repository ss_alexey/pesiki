import { Injectable } from '@angular/core';

// import * as WAValidator from 'wallet-address-validator';
import { HttpClient } from '@angular/common/http';
// import  from 'wallet-address-validator';
// import * as crypto from 'crypto';

@Injectable({
  providedIn: 'root'
})
export class WalletService {
  // address = '0x4961aC1d43B6249bc998D611F33d42B54E31712E';
  constructor(private http: HttpClient) { }
  validateWallet = (wallet: string, currency: string) => {
    if (currency === 'ETH' || currency === 'ETC') {
      return (/^(0x){1}[0-9a-fA-F]{40}$/i.test(wallet));
    }
    // BTC LTC DASH
    if (currency === 'BTC' || currency === 'LTC' || currency === 'DASH') {
      return wallet.length === 34;
      // return (/[0-9a-fA-F]{34}$/i.test(wallet));
    }
  }

}
