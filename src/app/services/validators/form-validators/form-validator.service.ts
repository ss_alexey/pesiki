import { Injectable } from '@angular/core';

import { WalletService } from '../wallet.service';
import { ValidatorFn, AbstractControl, FormGroup } from '@angular/forms';


@Injectable({
  providedIn: 'root'
})
export class FormValidatorService {
  constructor(private walletService: WalletService) { }
  validateWallet(wallet: string): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      const isInvalid = this.walletService.validateWallet(control.value, wallet);
      if (!isInvalid) {
        return { wallet: true };
      } else {
        return null;
      }
    };
  }
  validateIfSame(baseControl: string, validationControl: string): ValidatorFn {
    return (form: FormGroup): { [key: string]: any } | null => {
      if (form.controls[baseControl].value !== form.controls[validationControl].value) {
        form.controls[validationControl].setErrors({ equal: baseControl });
        return { equal: baseControl };
      } else {
        return null;
      }
    };
  }
}
