import { Injectable } from '@angular/core';
import { currencyWallet } from '../config';
import { ReplaySubject } from 'rxjs';

import { TransactionParams } from '@interfaces';

import { UserService } from './user.service';

// export interface Payment {
//   id: string;
//   value: number;
//   qrImg: string;
//   CDS: number;
//   wallet: string;
//   date?: number;
//   status?: string;
// }
@Injectable({
  providedIn: 'root'
})
export class QrCodeService {
  public paymentDetails = new ReplaySubject<TransactionParams>();
  constructor(private userService: UserService) { }
  public generateQrImage = (currency: string, amount?: number, gas?: number): string => {
    const qrUrl = 'https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl=';
    switch (currency) {
      case 'BTC':
        if (!amount) {
          return `${qrUrl}bitcoin:${currencyWallet.BTC}`;
        }
        return `${qrUrl}bitcoin:${currencyWallet.BTC}?amount=${amount}`;
      case 'LTC':
        if (!amount) {
          return `${qrUrl}litecoin:${currencyWallet.LTC}`;
        }
        return `${qrUrl}litecoin:${currencyWallet.LTC}?amount=${amount}`;
      case 'ETH':
        if (!amount) {
          return `${qrUrl}ethereum:${currencyWallet.ETH}`;
        }
        return `${qrUrl}ethereum:${currencyWallet.ETH}?amount=${amount}&gas=${gas}`;
      case 'BCH':
        if (!amount) {
          return `${qrUrl}bitcoincash:${currencyWallet.BCH}`;
        }
        return `${qrUrl}bitcoincash:${currencyWallet.BCH}?amount=${amount}`;
      case 'DASH':
        if (!amount) {
          return `${qrUrl}dash:${currencyWallet.DASH}`;
        }
        return `${qrUrl}dash:${currencyWallet.DASH}?amount=${amount}`;
      case 'ETC':
        if (!amount) {
          return `${qrUrl}ethereum:${currencyWallet.ETC}`;
        }
        return `${qrUrl}ethereum:${currencyWallet.ETC}?amount=${amount}`;
      default:
        return 'https://fivera.net/wp-content/uploads/2014/03/error_z0my4n.png';
    }
  }

  public updatePayment = (id: string, value: number, CDS: number) => {
    console.log('UPDATE');
    this.paymentDetails.complete();
    const payment: TransactionParams = {
      currency: id,
      amount: value,
      cdsToken: CDS,
      status: -1,
      user_id: this.userService.userParams && this.userService.userParams.ID || 1,
      date: '',
      addressFrom: this.userService.userParams && this.userService.userParams.Email,
      addressTo: `${currencyWallet[id]}`
    };
    this.paymentDetails.next(payment);
  }
  public setPayment = (payment: TransactionParams) => {
    this.paymentDetails.next(payment);
  }
}
