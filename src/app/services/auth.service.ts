import { Injectable } from '@angular/core';



import { LoginUser, LoginResponse, RegisterUser, RegisterResponse, UserParams } from '@interfaces';

import { ApiService } from './api.service';
import { UserService } from './user.service';
import { environment } from '@/environments/environment';
import { Router } from '@angular/router';

import * as jwtDecode from 'jwt-decode';

// tslint:disable-next-line:max-line-length
const token = 'eyJleHBpcmVzT24iOiI2MzY5NDkxMTYwNzUyMDk5NzAiLCJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJJRCI6MjQ0LCJFWFAiOjAsIlVzZXJuYW1lIjoiQWRtaW4iLCJCYWxhbmNlX0xUQyI6MCwiRWFybmVkX0xUQyI6MCwiRW1haWwiOiJhZG1pbkBhZG1pbi5jb20iLCJQQVNTX0hBU0giOiI1NUFCMDhCNCIsIlBob25lIjoiIiwiQXZhdGFyIjozNDE4NiwiQ0RTIjowLCJTdGF0dXMiOjEsIlJhbmsiOiJBd2FyZHMgMCAtIE5ld2JpZSIsIk93bmVkRG9ncyI6W10sImFjaGlldmVtZW50cyI6bnVsbCwiUmVmZXJyZWRfQnkiOm51bGx9.iWQl-IujPBIwgmFYQZ_notbeEhQ_aPdWR3wiKEZ4hYs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private apiService: ApiService, private userService: UserService, private router: Router) {
  }
  isAuthenticated() {
    if (localStorage.getItem('token')) {
      return true;
    }
    return false;
  }
  login = (userLogin: LoginUser) => {
    console.log('REMOVE FAKE LOGIN');
    // console.log(userLogin);
    if (userLogin.uid === '1' && userLogin.password === '1') {
      localStorage.setItem('token', token);
      this.userService.updateUser();
      this.router.navigate(['/']);
    }
    console.log(token);

    return this.apiService.post('login/', userLogin).toPromise().then((res: LoginResponse) => {
      // console.log(this);
      if (res && res.Token) {
        localStorage.setItem('token', res.Token);
        this.userService.updateUser();
      }
      // if (!environment.production) {
      //   return;
      // }
      this.router.navigate(['dashboard']);
    }).catch(e => {
      console.log(e);
    });
  }
  register = (userRegister: RegisterUser) => {


    return this.apiService.post('registration/', userRegister);
    // .toPromise().then((res: RegisterResponse) => {
    //   // if (res && res.Token) {
    //   //   localStorage.setItem('token', res.Token);
    //   // }
    //   // this.userService.updateUser();
    //   this.router.navigate(['auth']);
    // }).catch(e => {

    //   console.log(e);
    // });
  }
  logout = () => {
    // this.userService.updateUser(null);
    localStorage.removeItem('token');
    this.router.navigate(['auth']);
  }
}
