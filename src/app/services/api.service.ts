import { Injectable, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

import { apiUrl } from '@config';

import { FloatingErrorService } from './errorHandler';
import { Observable } from 'rxjs';

interface HttpOptions {

  headers?: HttpHeaders | {
    [header: string]: string | string[];
  };
  observe?: 'body';
  params?: HttpParams | {
    [param: string]: string | string[];
  };
  reportProgress?: boolean;
  responseType?: 'json';
  withCredentials?: boolean;

}
@Injectable({
  providedIn: 'root'
})
export class ApiService {
  token = localStorage.getItem('token');
  baseUrl = apiUrl;
  constructor(private http: HttpClient, private floatService: FloatingErrorService) {
  }
  post = (url: string, body: any, options?: HttpOptions) => {
    if (!this.token) {
      this.token = localStorage.getItem('token');
    }
    try {
      return this.http.post(`${this.baseUrl}${url}`, body, {
        headers: {
          Authorization: `Bearer ${this.token}`
        }
      });
    } catch (error) {
      console.log('ERRORRRRRRR');

    }
  }
  get = (url) => {
    if (!this.token) {
      this.token = localStorage.getItem('token');
    }
    return this.http.get(`${this.baseUrl}${url}`, {
      headers: {
        Authorization: `Bearer ${this.token}`
      }
    });

  }
}
