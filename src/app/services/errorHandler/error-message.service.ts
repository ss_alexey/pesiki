import { Injectable } from '@angular/core';
import { AbstractControl } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class ErrorMessageService {

  constructor() { }
  getErrorMessage = (control: AbstractControl) => {
    const errorKey = Object.keys(control.errors)[0];
    switch (errorKey) {
      case 'required':
        return 'this field is required';
      case 'email':
        return 'that`s not email';
      case 'equal':
        return `must be same as ${control.errors[errorKey]}`;
      case 'minlength':
        return `too short. Not lesser than ${control.errors[errorKey].requiredLength}`;
      default:
        console.log(errorKey);

        return 'unknown error';
    }
    // return errorKey;
  }
}
