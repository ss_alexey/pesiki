import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FloatingErrorService {
  notificationList = [];
  notifications = new Subject();
  constructor() { }
  addNotification = (status: number, text: string) => {
    this.notificationList.push({ status, text });
    this.notifications.next(this.notificationList);
  }
  removeNotification(index: number) {
    this.notificationList.splice(index, 1);
  }
}
