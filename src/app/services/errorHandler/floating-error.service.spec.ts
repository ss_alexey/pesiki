import { TestBed } from '@angular/core/testing';

import { FloatingErrorService } from './floating-error.service';

describe('FloatingErrorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FloatingErrorService = TestBed.get(FloatingErrorService);
    expect(service).toBeTruthy();
  });
});
